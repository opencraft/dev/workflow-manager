# Include local overrides to options.
# You can use this file to configure your dev env. It is ignored by git.
-include options.local.mk  # Prefix with hyphen to tolerate absence of file.

.DEFAULT_GOAL := oneshot
HELP_SPACING ?= 30
PACKAGE_NAME := listaflow
SOURCE_TAG ?= $(TAG)

# Parameters ##################################################################
#
# For ci commmands use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),ci-test-image-exists ci-pull-image ci-build-image ci-retag-django-image ci-push-images))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


BACKEND_SHELL_PREFIX=docker compose exec -e DJANGO_SETTINGS_MODULE=conf.settings.local django
FRONTEND_SHELL_PREFIX=docker compose exec frontend
BACKEND_SHELL_NO_TTY_PREFIX=docker compose exec -T -e DJANGO_SETTINGS_MODULE=conf.settings.local django
FRONTEND_SHELL_NO_TTY_PREFIX=docker compose run --rm -T frontend
BACKEND_HOME=/app/backend
FRONTEND_HOME=/app/frontend

help: ## Display this help message.
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-$(HELP_SPACING)s\033[0m %s\n", $$1, $$2}'

build: ## Build the docker images
	docker compose build --no-cache

install_frontend_prereqs:
	docker compose run --rm frontend npm --prefix /app/frontend install

# Celery beat needs to run after migration.
up: up.core migrate up.celery install_frontend_prereqs up.frontend ## start development server in daemon mode

restart: ## restart development server
	docker compose restart django celeryworker celerybeat

up.core: ## start development server without celery worker
	docker compose up -d

up.celery: ## start celery
	docker compose --profile celery up -d

up.frontend:
	docker compose up frontend -d

run: run.core migrate run.celery run.frontend ## start development server in foreground

run.core: ## start development server without_celery in foreground
	docker compose up

run.celery: ## start celery in foreground
	docker compose --profile celery up

logs: ## tail logs
	docker compose --profile celery logs -f --tail=300

run.frontend: ## start frontend development server
	docker compose up frontend

stop: ## stop all services
	docker compose --profile celery --profile frontend stop

createapplication: ## createapplication management command to create a new application
	${BACKEND_SHELL_PREFIX} ./manage.py createoauthapplication confidential password --skip-authorization --name main-app

seed_data: ## Populate with sample data
	${BACKEND_SHELL_NO_TTY_PREFIX} ./manage.py seed_data

provision: migrate collectstatic compilemessages.backend ## provision database

migrate: ## run migration
	${BACKEND_SHELL_NO_TTY_PREFIX} ./manage.py migrate

collectstatic: ## run collectstatic
	${BACKEND_SHELL_PREFIX} ./manage.py collectstatic -v0 --noinput

makemessages.backend: ## run makemessages for backend
	${BACKEND_SHELL_PREFIX} ./manage.py makemessages --all

compilemessages.backend: ## run compilemessages for backend
	${BACKEND_SHELL_PREFIX} ./manage.py compilemessages

makemessages.frontend: ## run makemessages for frontend
	${FRONTEND_SHELL_NO_TTY_PREFIX} npm --prefix /app/frontend run sync-i18n

shell: ## Open a shell on the backend container
	${BACKEND_SHELL_PREFIX} /bin/bash

destroy: ## stop and delete all containers
	docker compose --profile celery --profile frontend down

configure_frontend: ## Add this environment variable to the .env.local file if it doesn't exist.
	echo "REACT_APP_API_BASE_URL=http://localhost:8000" > frontend/.env.local

oneshot: up.core migrate up.celery install_frontend_prereqs configure_frontend up.frontend createapplication seed_data build.frontend collectstatic compilemessages.backend ## One shot command to get a local server up and running
	echo "Visit Listaflow at http://localhost:8000"

upgrade: ## upgrade python dependencies
	${BACKEND_SHELL_PREFIX} pip install pip-tools
	${BACKEND_SHELL_PREFIX} python -m piptools compile --upgrade --resolver=backtracking requirements/requirements.in requirements/constraints.in --output-file=requirements.txt

format: format.backend format.frontend ## format frontend and backend code

format.backend: ## format backend code
	${BACKEND_SHELL_PREFIX} python -m black .

format.frontend: ## format frontend code
	${FRONTEND_SHELL_NO_TTY_PREFIX} npm --prefix /app/frontend run format

quality.backend: ## run quality checks for backend code
	${BACKEND_SHELL_NO_TTY_PREFIX} python -m black --check .
	${BACKEND_SHELL_NO_TTY_PREFIX} python -m pylint --load-plugins=pylint_django ../backend

quality.frontend: ## Run quality checks for frontend code
	${FRONTEND_SHELL_NO_TTY_PREFIX} npm --prefix /app/frontend run check-format

quality: quality.backend quality.frontend ## run quality checks all code

test.backend: ## run quality checks for backend code
	${BACKEND_SHELL_NO_TTY_PREFIX} python -m pytest -n auto --cov-report html:../reports/coverage --cov-report term --cov --cov-fail-under=95 .

test.frontend: ## run tests for frontend code
	${FRONTEND_SHELL_NO_TTY_PREFIX} /bin/sh -c 'CI=true npm --prefix /app/frontend test --coverage'

test: test.backend test.frontend ## run tests for backend and frontend code

qa: quality test ## run all quality checks and tests

build.frontend: ## build frontend
	${FRONTEND_SHELL_NO_TTY_PREFIX} npm --prefix /app/frontend run build

# CI/CD #######################################################################
ci-test-image-exists:
	@export REPO_ID=`curl -s ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/ | jq '.[] | select(.location==env.CI_REGISTRY_IMAGE) | .id'`; \
	test -z $$REPO_ID && exit 0; \
	test `curl -sf -w "%{http_code}" -o /dev/null ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/$$REPO_ID/tags/${RUN_ARGS}-${VERSION}` = 404 || \
	(echo "Image version ${VERSION} already exists. Please bump up the version by incrementing the 'VERSION' variable in '.gitlab-ci.yml'." && exit 1)

ci-pull-image:
	docker pull "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" || docker pull "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-latest" || true

ci-build-image:
	docker build \
		--cache-from "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" \
		--cache-from "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-latest" \
		-t "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" \
		-f docker/backend/Dockerfile .

ci-retag-django-image:
	@for service in $(RUN_ARGS) ; do \
		docker tag "${CI_REGISTRY_IMAGE}:django-$(SOURCE_TAG)" "${CI_REGISTRY_IMAGE}:$$service-$(TAG)" ; \
	done

ci-push-images:
	@for service in $(RUN_ARGS) ; do \
		docker push "${CI_REGISTRY_IMAGE}:$$service-$(TAG)" ; \
	done
