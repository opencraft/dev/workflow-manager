"""
Django admin configuration for lib module.
"""

from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from nonrelated_inlines.admin import NonrelatedTabularInline
from short_stuff import unslugify

from lib.models import AuditLog


class LoggingInline(NonrelatedTabularInline):
    """
    Inline admin listing of audit logs for an object. Uses the targets m2m to determine
    which logs to show.
    """

    model = AuditLog

    readonly_fields = [field.name for field in AuditLog._meta.get_fields()]

    def has_add_permission(self, request, obj=None):
        """
        Audit logs should be read only.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Audit logs should be read only.
        """
        return False

    def save_new_instance(self, parent, instance):
        """
        Required for override on abstract base class.
        """

    def get_form_queryset(self, obj):
        pk = obj.pk
        if isinstance(obj.pk, str):
            pk = unslugify(pk)
        return AuditLog.objects.filter(
            targets__object_id=pk,
            targets__content_type=ContentType.objects.get_for_model(obj),
        )


@admin.register(AuditLog)
class AuditLogAdmin(admin.ModelAdmin):
    """
    List all audit logs.
    """

    readonly_fields = [
        field.name for field in AuditLog._meta.get_fields() if field.name != "targets"
    ]
    readonly_fields.append("get_targets")
    fields = readonly_fields
    list_display = ("event_type", "level", "created_on", "get_targets")
    search_fields = ("event_type", "level")
    list_filter = ("event_type", "level")
    sortable_by = ("created_on",)

    def get_targets(self, obj):
        """
        Render a useful display of
        :param obj:
        :return:
        """
        items = []
        for item in obj.targets.all():
            if item.target is None:
                items.append("<Deleted item>")
                continue
            items.append(str(item.target))
        return ", ".join(items)

    get_targets.short_description = "targets"
