"""
Django app configuration for lib.
"""

from django.apps import AppConfig


class LibConfig(AppConfig):
    """
    Lib configuration definition.
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "lib"
