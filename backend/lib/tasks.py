"""
Tasks unrelated to any particular functionality.
"""

import requests
from celery import shared_task
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.utils import timezone

from lib.models import AuditLog


@shared_task
def snitch_check_in():
    """
    Make sure that celery is working as expected by scheduling this task to ping your
    dead man's snitch account. Or whatever endpoint you're using for similar
    functionality.
    """
    if not settings.DEAD_MANS_SNITCH_URL:
        return
    response = requests.get(settings.DEAD_MANS_SNITCH_URL, timeout=5)
    response.raise_for_status()


@shared_task
def remove_old_logs():
    """
    Clear out old audit logs.
    """
    if not settings.AUDIT_LOG_PRESERVATION_DAYS:
        return
    AuditLog.objects.filter(
        created_on__lt=timezone.now()
        - relativedelta(days=settings.AUDIT_LOG_PRESERVATION_DAYS)
    ).delete()
