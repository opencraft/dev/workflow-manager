"""
Tests for lib helpers.
"""

from unittest.mock import patch

import pytest
from django.db import transaction

from lib.constants import ERROR, WARNING, INFO
from lib.helpers import log_event
from lib.models import AuditLog, ref_for_instance, GenericReference
from profiles.tests.factories import TeamFactory, UserFactory

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


def test_basic_case():
    """
    Test that the logging decorator creates an Audit log entry.
    """
    with log_event("do_thing"):
        pass
    log = AuditLog.objects.get()
    assert log.event_type == "do_thing"


def test_handles_error():
    """
    Test that a log entry is added even if an exception is raised.
    """
    with pytest.raises(ValueError), log_event("process_stuff"):
        TeamFactory.create()
        raise ValueError("Uh oh!")

    # The audit log's saving should have been scheduled for after the transaction:
    log = AuditLog.objects.get()
    assert log.event_type == "process_stuff"
    assert log.data["exception"] == "ValueError: Uh oh!"
    assert log.data["traceback"]


@patch("lib.helpers.LOG_FUNCS")
def test_logs_to_console_even_with_broken_transaction(mock_funcs):
    """
    Test that we still log to console if there's a broken transaction.
    """
    with pytest.raises(ValueError), transaction.atomic(), log_event("compute"):
        mock_funcs[ERROR].assert_not_called()
        raise ValueError("Oh no!")

    # Transaction would prevent an AuditLog from being created. Verify this in case
    # something major changes:
    assert AuditLog.objects.count() == 0

    mock_funcs[ERROR].assert_called()


def test_handles_targets():
    """
    Test that we handle targets sanely.
    """
    team = TeamFactory.create()
    user = UserFactory.create()
    user2 = ref_for_instance(UserFactory.create())
    with pytest.raises(ValueError), log_event("stuff", targets=[team, user, user2]):
        raise ValueError("Oops!")

    log = AuditLog.objects.get()

    def key(ref: GenericReference):
        return ref.id

    assert sorted(list(log.targets.all()), key=key) == sorted(
        [
            ref_for_instance(team),
            ref_for_instance(user),
            ref_for_instance(user2.target),
        ],
        key=key,
    )


def test_custom_log_level():
    """
    Test that setting a specific log level is respected.
    """
    with log_event("warn", level=WARNING):
        pass
    log = AuditLog.objects.get()

    assert log.level == WARNING


def test_custom_error_level():
    """
    Test that a custom error level is respected.
    """
    with pytest.raises(ValueError), log_event(
        "do_stuff", level=INFO, fail_level=WARNING
    ):
        raise ValueError("Err!")

    log = AuditLog.objects.get()

    assert log.level == WARNING


def test_data_handling():
    """
    Test that adding data is respected
    """
    with log_event("do_things", data={"stuff": "things"}):
        pass

    log = AuditLog.objects.get()

    assert log.data == {"stuff": "things"}


@pytest.mark.parametrize(
    "slug",
    [
        "slug thing",
        "@$#!Q$",
        "beep_boop_beep_boop_beep_boop_beep_boop_beep_boop_beep_boop_beep_boop_beep",
        "",
    ],
)
def test_slug_check(slug):
    """
    Test that putting a nonsense slug throws.
    """
    with pytest.raises(ValueError):
        with log_event(slug):
            pass
