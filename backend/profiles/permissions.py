"""
Django REST Framework Permissions classes for profiles.
"""

from django.db.models import Model
from rest_framework.permissions import BasePermission

from profiles.models import TeamMembership, Team


def get_team(obj: Model):
    """
    Derives the relevant team from an object. This might need expansion depending
    on what models we end up handing it.
    """
    if isinstance(obj, Team):
        return obj
    if hasattr(obj, "team"):
        return obj.team
    raise TypeError(f"Could not determine related team for obj {obj}")


class IsTeamMember(BasePermission):
    """
    Checks if the requester is a member of the team being queried.
    """

    def has_permission(self, request, view):
        """
        Performs a view-level team check.

        If we're examining a nested Viewset for teams, we may not have an object to
        check if we're in the global listing. In this case, we need to check via team_id
        in the view kwargs.

        Views must have the relevant kwarg for this permission to work at the view,
        and models must have a derivable team (per get_team) for it to work at the
        object permissions level.
        """
        if "team_id" in view.kwargs:
            return TeamMembership.objects.filter(
                team_id=view.kwargs["team_id"], user=request.user
            ).exists()
        # Need to check via has_object_permission, but this shouldn't be a failure
        # if not relevant.
        return True

    def has_object_permission(self, request, view, obj):
        """
        Performs the check.
        """
        return get_team(obj).members.filter(id=request.user.id).exists()
