"""
Tests for profiles view module.
"""

from unittest.mock import patch

from django.core import mail
from django.urls import reverse

import pytest
from faker import Faker
from rest_framework import status

from profiles.tests.factories import (
    ApplicationFactory,
    TeamFactory,
    UserFactory,
    TeamMembershipFactory,
)

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("api_client", "celery_worker", "patch_flush_many_cache"),
]

fake = Faker()


def test_resend_verification_email(*, api_client):
    """
    Test resend email verification when user is not marked as active
    """
    client = api_client()
    user = UserFactory()
    user.is_active = False
    user.save()
    response = client.post(
        reverse("profiles:resend-verification-email"), {"id": user.pk}
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(mail.outbox) > 0
    assert "Please confirm your email" in mail.outbox[0].subject


def test_should_not_resend_verification_email(*, api_client):
    """
    Test resend email verification when user is marked as active
    """
    client = api_client()
    user = UserFactory()
    response = client.post(
        reverse("profiles:resend-verification-email"), {"id": user.pk}
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(mail.outbox) == 0


@patch("profiles.views.process_verify_registration_data")
def test_verify_registration(mock_verify, api_client):
    """
    Test auto login on verify registration
    """
    client = api_client()
    application = ApplicationFactory()
    user = UserFactory()
    user.is_active = False
    user.save()
    mock_verify.return_value = user
    response = client.post(
        reverse("profiles:verify-registration"),
        {
            "client_id": application.client_id,
            "client_secret": application.client_secret,
            "user_id": user.pk,
            "timestamp": 123,
            "signature": "some-signature",
        },
    )
    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert "access_token" in data
    assert "refresh_token" in data


def test_fetch_team(*, api_client):
    """Test fetch user's team"""
    client = api_client()
    user = UserFactory()
    user.is_active = True
    user.save()
    team1 = TeamFactory()
    team2 = TeamFactory()

    url = reverse("profiles:account-teams")

    client.force_login(user)

    team1.members.add(user)
    team1.save()

    response = client.get(url)
    assert len(response.data) == 1

    team2.members.add(user)
    team2.save()

    response = client.get(url)
    assert len(response.data) == 2


def test_fetch_team_names(*, api_client):
    """Test fetch user's team"""
    client = api_client()
    user = UserFactory()
    user.is_active = True
    user.save()
    team1 = TeamFactory()
    team2 = TeamFactory()

    url = reverse("profiles:account-team-names")

    client.force_login(user)

    team1.members.add(user)
    team1.save()

    response = client.get(url)
    assert len(response.data) == 1

    team2.members.add(user)
    team2.save()

    response = client.get(url)
    assert len(response.data) == 2

    assert list(response.data[0].keys()) == ["id", "name"]


def test_list_global_teams(*, api_client):
    """
    Test the global teams listing.
    """
    client = api_client()
    team = TeamFactory()
    url = reverse("profiles:teams-list")
    user = UserFactory.create(is_superuser=True)
    client.force_login(user)
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data["results"]) == 1
    assert response.data["results"][0]["id"] == team.id


def test_list_team_memberships(*, api_client):
    """
    Test that we can list existing team memberships.
    """
    client = api_client()
    membership = TeamMembershipFactory(tags=["Beep", "Boop"])
    client.force_login(membership.user)
    url = reverse("profiles:memberships-list", kwargs={"team_id": membership.team.id})
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert list(sorted(response.data["results"][0]["tags"])) == ["Beep", "Boop"]


def test_non_member_memberships_denied(*, api_client):
    """
    Test that non-team members can't view memberships.
    """
    client = api_client()
    membership = TeamMembershipFactory()
    client.force_login(UserFactory())
    url = reverse("profiles:memberships-list", kwargs={"team_id": membership.team.id})
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_retrieve_one(*, api_client):
    """
    Test retrieval of a specific membership.
    """
    client = api_client()
    membership = TeamMembershipFactory(tags=["Beep", "Boop"])
    client.force_login(membership.user)
    url = reverse(
        "profiles:memberships-detail",
        kwargs={"team_id": membership.team.id, "membership_id": membership.id},
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert list(sorted(response.data["tags"])) == ["Beep", "Boop"]


def test_retrieve_one_fails_non_member(*, api_client):
    """
    Test retrieval of a specific membership fails for unrelated user.
    """
    client = api_client()
    membership = TeamMembershipFactory()
    client.force_login(UserFactory())
    url = reverse(
        "profiles:memberships-detail",
        kwargs={"team_id": membership.team.id, "membership_id": membership.id},
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_update_membership(*, api_client):
    """
    Test updating the fields on a membership.
    """
    client = api_client()
    membership = TeamMembershipFactory(tags=["Beep", "Boop"])
    client.force_login(membership.user)
    url = reverse(
        "profiles:memberships-detail",
        kwargs={"team_id": membership.team.id, "membership_id": membership.id},
    )
    response = client.patch(
        url,
        {
            "tags": ["Test", "tag", "set"],
            "im_handle": "Beep",
            "team": "invalid",
            "user": "also invalid",
        },
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data["im_handle"] == "Beep"
    assert response.data["user"]["id"] == membership.user.id
    assert response.data["team_id"] == membership.team.id
    assert list(sorted(response.data["tags"])) == ["Test", "set", "tag"]
    assert list(sorted([tag.name for tag in membership.tags.all()])) == [
        "Test",
        "set",
        "tag",
    ]
