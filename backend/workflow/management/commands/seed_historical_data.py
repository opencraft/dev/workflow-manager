from datetime import datetime, timedelta
import random
from math import floor, ceil
from typing import Union, Optional

from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone

from faker import Faker

from workflow.interaction import (
    MARKDOWN_TEXTAREA_INTERFACE,
    LINEAR_SCALE_RATING_INTERFACE,
    CHECKBOX_INTERFACE,
    NUMERIC_INTERFACE,
    MULTIPLE_CHOICE_INTERFACE,
    SUBSECTION_INTERFACE,
)
from workflow.models import Recurrence, Run, Checklist, ChecklistTask

fake = Faker()


def create_historical_run(
    last_start_date: datetime,
    recurrence: Recurrence,
) -> (datetime, Run):
    """
    Creates a backdated historical run of a recurrence, based on a particular
    start date.
    """
    schedule = recurrence.interval_schedule.schedule
    new_start_date = last_start_date - schedule.run_every
    run = recurrence.create_run(
        start_date=new_start_date,
        end_date=last_start_date,
        due_date=last_start_date.date(),
    )
    return new_start_date, run


def mock_checkbox(task: ChecklistTask, *, sentiment: int, **_kwargs):
    """
    Marks a checkbox task as complete.
    """
    if task.required:
        task.response = {"checked": True}
        return
    result = random.randrange(1, 100)
    task.response = {"checked": result < sentiment}


def mock_markdown_textarea(task: ChecklistTask, **_kwargs):
    """
    Fills a textarea with mock info.
    """
    task.response = {"content": fake.sentence()}


AnyNumber = Union[int, float]


def range_sentiment_subset(
    *,
    min_value: int,
    max_value: int,
    step: int,
    sentiment: AnyNumber,
    chaos: int,
):
    """
    When we have all relevant values, return a max and minimum value for a range of
    numeric responses.
    """
    # Now that we have a sane minimum and maximum, we need to resolve values that will
    # be the minimum and maximum responses from the users based on the sentiment and
    # chaos.
    possibilities = list(range(min_value, max_value + 1, step))
    base_value = (sentiment / 100.0) * len(possibilities)
    modifier = (chaos / 2.0 / 100.0) * len(possibilities)
    min_index = max(int(floor(base_value - modifier)), 1) - 1
    max_index = min(int(ceil(base_value + modifier)), len(possibilities)) - 1
    return possibilities[min_index], possibilities[max_index]


def default_range_parameters(
    *,
    min_value: Optional[int],
    max_value: Optional[int],
    step: AnyNumber,
    sentiment: AnyNumber = 85,
    chaos: AnyNumber = 20,
):
    """
    Factored out to be more testable. Given optional min, max, and step values,
    alongside a sentiment and chaos value, come up with a sane min and max.

    This assumes the values at least passed validation elsewhere upon saving.
    """
    if min_value is None and max_value is not None:
        min_value = max_value - chaos
    if max_value is None and min_value is not None:
        max_value = min_value + chaos
    # If either are still None, then both were None. Pick default ranges near the
    # sentiment value with a gap of chaos, and make sure they are on valid steps apart.
    modifier = chaos / 2.0
    if min_value is None:
        base_number = sentiment - modifier
        min_value = base_number - step + (base_number % step)
    if max_value is None:
        base_number = sentiment + modifier
        max_value = base_number + (step - (base_number % step))
    min_value = int(min_value)
    max_value = int(max_value)
    return range_sentiment_subset(
        min_value=min_value,
        max_value=max_value,
        step=step,
        chaos=chaos,
        sentiment=sentiment,
    )


def mock_numeric(task: ChecklistTask, sentiment=85, chaos=20, **_kwargs):
    """
    Mock a numeric task response.
    """
    defs = task.definition.customization_args
    step = defs.get("step") or 1
    max_value = defs.get("max_value")
    min_value = defs.get("min_value")
    checks = [
        isinstance(step, float),
        isinstance(max_value, float),
        isinstance(min_value, float),
    ]
    if any(checks):
        # Let's not make the calculations needlessly difficult.
        raise TypeError(
            f"Task {task.definition.id} has one of its parameters "
            f"configured as a float. Only integers are supported for "
            f"historical data generation."
        )
    min_value, max_value = default_range_parameters(
        min_value=min_value,
        max_value=max_value,
        step=step,
        sentiment=sentiment,
        chaos=chaos,
    )
    if min_value == max_value:
        number = min_value
    else:
        number = random.randrange(min_value, max_value + 1, step)
    task.response = {"number": number}


def mock_linear(
    task: ChecklistTask, *, sentiment: AnyNumber, chaos: AnyNumber, **_kwargs
):
    """
    Mock a response to a linear task.
    """
    defs = task.definition.customization_args
    min_value = defs.get("min_value")
    max_value = defs.get("max_value")
    bottom, top = range_sentiment_subset(
        min_value=min_value,
        max_value=max_value,
        step=1,
        sentiment=sentiment,
        chaos=chaos,
    )
    if top == bottom:
        # Not enough chaos/range/options to do any more than one value.
        task.response = {"rating": top}
    else:
        task.response = {"rating": random.randrange(bottom, top)}


def mock_multiple_choice(task: ChecklistTask, **_kwargs):
    """
    Pick some amount of the multiple choice options between the minimum and maximum
    number of options. We don't use sentiment/chaos for this since it's not obvious
    what it should do here.
    """
    defs = task.definition.customization_args
    options = defs.get("available_choices")
    minimum_selected = defs.get("minimum_selected", 0)
    maximum_selected = defs.get("maximum_selected")
    if maximum_selected is None:
        maximum_selected = len(options)
    response = []
    # Task definition validation will prevent the available choices from being
    # fewer than the minimum, so no need to check here.
    for selection in range(0, random.randrange(minimum_selected, maximum_selected)):
        response.append(options.pop(random.randrange(0, len(options) - 1)))
    task.response = {"selected_choices": response}


def mock_subsection_interface(_task: ChecklistTask, **_kwargs):
    """
    Subsections do not, themselves, have anything to interact with.
    We just mark them as complete because they will be anyway.
    """


ACTIONS = {
    MARKDOWN_TEXTAREA_INTERFACE: mock_markdown_textarea,
    LINEAR_SCALE_RATING_INTERFACE: mock_linear,
    CHECKBOX_INTERFACE: mock_checkbox,
    NUMERIC_INTERFACE: mock_numeric,
    MULTIPLE_CHOICE_INTERFACE: mock_multiple_choice,
    SUBSECTION_INTERFACE: lambda *args, **kwargs: None,
}


def random_datetime(start: datetime, end: datetime):
    """
    This function will return a random datetime between two datetime
    objects.

    Yoinked and modified from https://stackoverflow.com/a/553448/927224
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


def build_mock_submissions(
    run: Run,
    sentiment=85,
    compliance_rate=80,
    chaos=30,
    seed=None,
):
    """
    Build a set of mock submissions based on the tuned factors above.

    Factor numbers should be between 0 and 100, as they are percentages.
    """
    members = list(run.team.members.all())
    random.seed(seed)
    for member in members:
        completed_on = random_datetime(run.start_date, run.end_date)
        if random.randint(0, 100) > compliance_rate:
            continue
        checklist = Checklist.objects.get(run=run, assignee=member)
        to_update = []
        for task in checklist.active_tasks():
            ACTIONS[task.definition.interface_type](
                task,
                sentiment=sentiment,
                compliance_rate=compliance_rate,
                chaos=chaos,
            )
            task.completed = True
            task.completed_on = completed_on
            to_update.append(task)
        ChecklistTask.objects.bulk_update(
            to_update,
            ["completed", "completed_on", "response"],
        )
        checklist.completed = True
        checklist.completed_on = completed_on
        checklist.save()


class Command(BaseCommand):
    help = "Given a recurrence, generate fake historical data for that recurrence."

    def add_arguments(self, parser):
        parser.add_argument(
            "--recurrence",
            type=str,
            help="The ID of the recurrence to create historical data for.",
        )
        parser.add_argument(
            "--run-count",
            default=12,
            type=int,
            help="How many historical runs to create. That is, how many periods "
            "of the recurrence to go back and fill in. The count will start from "
            "the oldest existing recurrence, or the start time of the recurrence "
            "if there is not one.",
        )
        parser.add_argument(
            "--compliance-rate",
            help="The average compliance rate for the historical data-- that is, what "
            "percent of the team members fill out the checklist--from 1 to 100. ",
            default=80,
            type=int,
        )
        parser.add_argument(
            "--sentiment",
            help="An integer percentage, from 1 to 100, expressing the general "
            "sentiment for rating questions.",
            default=85,
            type=int,
        )
        parser.add_argument(
            "--chaos",
            help="An integer number, from 1 to 10, expressing how much chaos to add to "
            "the data-- that is, how closely it conforms to the sentiment value.",
            default=30,
            type=int,
        )
        parser.add_argument(
            "--seed",
            help="Seed data to be used for the pseudorandom generator. Use if you need "
            "reproducible data for some reason.",
            default=None,
            type=str,
        )

    @transaction.atomic()
    def handle(self, *args, **options):
        try:
            recurrence = Recurrence.objects.get(id=options["recurrence"])
        except (ValueError, Recurrence.DoesNotExist) as err:
            self.stderr.write(
                f"Error loading Recurrence with id {options['recurrence']}: {err}"
            )
            return
        if not recurrence.interval_schedule:
            self.stderr.write(
                f"Only recurrences with interval schedules are supported for "
                f"this feature."
            )
            return
        first_run = recurrence.runs.order_by("start_date").first()
        if first_run:
            start_date = first_run.start_date
        else:
            start_date = recurrence.start_date or (
                timezone.now() + recurrence.interval_schedule.schedule.run_every
            )
        for i in range(0, options["run_count"]):
            start_date, resulting_run = create_historical_run(
                start_date,
                recurrence,
            )
            # Bailing early while testing. Remove this.
            try:
                build_mock_submissions(
                    resulting_run,
                    sentiment=options["sentiment"],
                    compliance_rate=options["compliance_rate"],
                    chaos=options["chaos"],
                    seed=options["seed"],
                )
            except TypeError as err:
                self.stderr.write(str(err))
                return
