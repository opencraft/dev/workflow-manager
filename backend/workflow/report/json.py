"""
Handle caching and CSV writing logic for reports
"""

from utils.cache import get_cache, make_cache_key, set_cache

from workflow.report.base import CacheModelReportBase
from workflow.serializers import (
    ChecklistDefinitionTrendsSerializer,
    RunOverviewSerializer,
    ChecklistTaskResponseSerializer,
)


class JsonReportBase(CacheModelReportBase):
    """
    Base for reports based on models
    """

    serializer_class: type

    def __init__(self, *args, **kwargs):
        self.is_many = kwargs.get("is_many", False)
        self.context = kwargs.get("context", {})

    def get_serializer_class(self):
        """
        Get serializer class for report
        """
        assert self.serializer_class is not None, (
            f"{self.__class__.__name__} should either include a `serializer_class` "
            "attribute, or override the `get_serializer_class()` method."
        )
        return self.serializer_class

    def __get_single_serialized_data(self, instance):
        """
        Get and cache data for instance
        Args:
            instance: ModelInstance. instance model for getting data from
        """
        cache_key = self.make_cache_key(instance, self.context)
        cache_data = get_cache(cache_key)
        if cache_data:
            return cache_data
        serializer_class = self.get_serializer_class()
        data = serializer_class(instance=instance, context=self.context).data
        set_cache(cache_key, data)
        return data

    def get_json_data(self):
        """
        Get serialized data for the report
        """

        instance = self.get_instance()
        is_many = self.is_many
        data = None
        if is_many:
            data: list = []
            for _instance in instance:
                _data = self.__get_single_serialized_data(_instance)
                data.append(_data)
        else:
            data = self.__get_single_serialized_data(instance)
        return data


class TemplateRunsJsonReport(JsonReportBase):
    """
    Generate Recurrence Run json report
    """

    serializer_class = ChecklistTaskResponseSerializer

    def __init__(
        self,
        instance,
        *args,
        **kwargs,
    ):
        self.instance = instance
        super().__init__(*args, **kwargs)

    @classmethod
    def make_cache_prefix(cls):
        """
        Override to append trends cache to cache key suffix
        """
        original_suffix = super().make_cache_prefix()
        return make_cache_key(original_suffix, "Run")

    @staticmethod
    def group_tasks_by_definition_id(tasks: list) -> dict:
        """
        Get a map with tasks group by definition ID

        Args:
            checklists: list. A list of checklists
        """
        definition_id_map: dict = {}
        for task in tasks:
            definition_id_map.setdefault(
                task["definition_id"],
                {"completed": 0, "total": 0, "dates": set([]), "tasks": {}},
            )
            run_tasks = definition_id_map[task["definition_id"]]["tasks"]
            date_header = f"{task['start_date']} - {task['end_date']}"
            run_tasks.setdefault(task["assignee_username"], {})
            run_tasks[task["assignee_username"]][date_header] = task["response"]
            run_tasks[task["assignee_username"]]["assignee_name"] = task[
                "assignee_name"
            ]
            stat_dict = definition_id_map[task["definition_id"]]
            if task["response"]:
                stat_dict["completed"] += 1
            stat_dict["total"] += 1
            stat_dict["interface_type"] = task["interface_type"]
            stat_dict["task_label"] = task["task_label"]
            stat_dict["dates"].add(date_header)
        for definition_map in definition_id_map.values():
            definition_map["dates"] = sorted(definition_map["dates"], reverse=True)
        return definition_id_map


class ChecklistDefinitionTrendsJsonReport(JsonReportBase):
    """
    Generate ChecklistDefinition trend report
    """

    serializer_class = ChecklistDefinitionTrendsSerializer

    def __init__(self, instance, *args, **kwargs):
        self.instance = instance
        super().__init__(*args, **kwargs)

    @classmethod
    def make_cache_suffix(cls):
        """
        Override to append trends cache to cache key suffix
        """
        original_suffix = super().make_cache_suffix()
        return make_cache_key(original_suffix, "trends")


class RunOverviewJsonReport(JsonReportBase):
    """
    Generate ChecklistDefinition trend report
    """

    serializer_class = RunOverviewSerializer

    def __init__(self, instance, *args, **kwargs):
        self.instance = instance
        self.context = kwargs.get("context", {})
        super().__init__(*args, **kwargs)

    @classmethod
    def make_cache_suffix(cls):
        """
        Override to append trends cache to cache key suffix
        """
        original_suffix = super().make_cache_suffix()
        return make_cache_key(original_suffix, "overview")
