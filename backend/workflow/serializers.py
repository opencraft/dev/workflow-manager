"""
Serializers for workflows.
"""

from typing import Any
from django.db.models import Prefetch
from django.utils.translation import ngettext

from rest_framework import serializers
from rest_framework_bulk import serializers as bulk_serializers
from taggit.serializers import TaggitSerializer, TagListSerializerField

from profiles.models import TeamMembership
from profiles.serializers import UserSerializer
from utils.exceptions import raise_rest_validation_error
from workflow.enums import ChecklistArchiveAction
from workflow.interaction import (
    INTERFACE_TYPE_TO_CLASS,
    LINEAR_SCALE_RATING_INTERFACE,
    response_repr,
)
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    EventRecurrenceMapping,
    Recurrence,
    Run,
)


class ChecklistTaskDefinitionSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
    Serializer for task definitions.
    """

    tags = TagListSerializerField(required=False)

    class Meta:
        model = ChecklistTaskDefinition
        fields = (
            "id",
            "label",
            "body",
            "interface_type",
            "customization_args",
            "tags",
            "order",
        )
        read_only_fields = ("id",)


_checklist_task_fields_to_serialize = (
    "id",
    "label",
    "body",
    "completed",
    "completed_on",
    "required",
    "interface_type",
    "customization_args",
    "response",
    "response_display",
    "definition_id",
    "assignee",
    "parent",
    "tags",
)
_checklist_task_fields_to_serialize_as_read_only = (
    "id",
    "label",
    "body",
    "completed",
    "completed_on",
    "required",
    "interface_type",
    "customization_args",
    "response_display",
    "definition_id",
    "assignee",
    "parent",
    "tags",
)


class ChecklistTaskSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
    Serializer for tasks without children.
    """

    assignee = UserSerializer(read_only=True)
    tags = TagListSerializerField(required=False)

    class Meta:
        model = ChecklistTask
        fields = _checklist_task_fields_to_serialize
        read_only_fields = _checklist_task_fields_to_serialize_as_read_only

    def validate(self, attrs):
        if "response" in attrs:
            interface_class = INTERFACE_TYPE_TO_CLASS[self.instance.interface_type]
            interface_class.validate_response(
                attrs["response"], self.instance.customization_args
            )

            attrs["completed"] = interface_class.can_mark_completed(
                attrs["response"], self.instance.customization_args
            )
        return attrs


class ChecklistDefinitionListSerializer(serializers.ModelSerializer):
    """
    Serializer for Checklist Definitions.
    """

    class Meta:
        model = ChecklistDefinition
        fields = ("id", "name")
        read_only_fields = ("id", "name")


class ChecklistDefinitionSerializer(serializers.ModelSerializer):
    """
    Serializer for Checklist Definitions.
    """

    task_definitions = ChecklistTaskDefinitionSerializer(
        many=True,
        read_only=True,
        source="active_task_definitions",
    )

    class Meta:
        model = ChecklistDefinition
        fields = ("id", "body", "name", "task_definitions", "team")
        read_only_fields = ("id",)


class RecurrenceSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
    Serializer for Recurrence
    """

    tags = TagListSerializerField(required=False)

    class Meta:
        model = Recurrence
        fields = (
            "id",
            "periodic_task",
            "tags",
            "recurring_schedule_display",
            "team",
            "checklist_definition",
        )
        read_only_fields = fields


# pylint: disable=W0223
class RunSerializer(serializers.ModelSerializer):
    """
    Serializer for runs.
    """

    assignees = serializers.SerializerMethodField()
    recurrence = RecurrenceSerializer(read_only=True)

    def get_assignees(self, instance):
        """
        get all assignees username and display_name
        """
        assignees = []
        for checklist in instance.checklists.all():
            assignee_data = {
                "id": checklist.assignee.id,
                "username": checklist.assignee.username,
                "email": checklist.assignee.email,
                "display_name": checklist.assignee.display_name,
                "completed": checklist.completed,
            }
            assignees.append(assignee_data)
        return assignees

    class Meta:
        model = Run
        fields = (
            "id",
            "start_date",
            "end_date",
            "due_date",
            "name",
            "team_name",
            "is_archived",
            "completed_checklist_count",
            "total_checklist_count",
            "assignees",
            "recurrence",
        )


class RunUpdateSerializer(RunSerializer):
    """
    Serializer for updating runs. Only allows archiving and unarchiving.
    """

    class Meta(RunSerializer.Meta):
        extra_kwargs = {
            "id": {"read_only": True},
            "start_date": {"read_only": True},
            "end_date": {"read_only": True},
            "due_date": {"read_only": True},
            "is_archived": {"read_only": False},
        }


class ChecklistSerializer(serializers.ModelSerializer):
    """
    Serializer for checklists.
    """

    assignee = UserSerializer(read_only=True)
    run = RunSerializer(read_only=True)
    status = serializers.CharField(read_only=True)
    force_complete = serializers.BooleanField(default=False)

    def update(self, instance: Checklist, validated_data):
        """
        Update checklist, but don't allow changing of source definition.
        """
        if "definition" in validated_data:
            validated_data.pop("definition")
        completed = validated_data.get("completed")
        force_complete = validated_data.get("force_complete", False)
        if completed:
            status, pending_tasks = instance.complete(force=force_complete)
            if not status:
                msg = ngettext(
                    "Oops, {pending_tasks} required item still need to be completed",
                    "Oops, {pending_tasks} required items still need to be completed",
                    pending_tasks,
                ).format(pending_tasks=pending_tasks)
                raise_rest_validation_error("completed", msg)
            validated_data.pop("completed")
        return super().update(instance, validated_data)

    def create(self, validated_data: Any):
        if "force_complete" in validated_data:
            validated_data.pop("force_complete")
        return super().create(validated_data)

    class Meta:
        model = Checklist
        fields = (
            "id",
            "status",
            "name",
            "body",
            "definition",
            "created_on",
            "completed",
            "force_complete",
            "completed_on",
            "assignee",
            "run",
            "is_archived",
            "team",
        )
        read_only_fields = (
            "id",
            "created_on",
            "completed_on",
            "assignee",
            "run",
        )
        extra_kwargs = {"name": {"required": False}, "completed": {"required": False}}


class ChecklistReviewSerializer(ChecklistSerializer):
    """
    Serializer for short version of checklists, tasks list
    """

    def get_completed_task_count(self, obj):
        """
        Get checklist completed task count

        Args:
            obj: Checklist
        """
        return obj.completed_tasks.count()

    def get_total_task_count(self, obj):
        """
        Get checklist total task count

        Args:
            obj: Checklist
        """
        return obj.active_tasks().count()

    completed_task_count = serializers.SerializerMethodField()
    total_task_count = serializers.SerializerMethodField()

    class Meta:
        model = Checklist
        fields = (
            "id",
            "is_archived",
            "status",
            "name",
            "body",
            "created_on",
            "completed",
            "force_complete",
            "assignee",
            "run",
            "completed_task_count",
            "total_task_count",
        )
        read_only_fields = (
            "id",
            "name",
            "body",
            "created_on",
            "completed_task_count",
            "total_task_count",
        )
        list_serializer_class = bulk_serializers.BulkListSerializer


class CumulativeResponseRate(serializers.Serializer):
    """
    Serializer for response rate
    """

    completed_count = serializers.IntegerField()
    total_count = serializers.IntegerField()
    timestamp = serializers.DateTimeField(required=False)
    start_date = serializers.DateTimeField(required=False)
    end_date = serializers.DateTimeField(required=False)

    class Meta:
        fields = (
            "timestamp",
            "completed_count",
            "total_count",
            "start_date",
            "end_date",
        )


class NumericStats(serializers.Serializer):
    """
    Serializer for numeric questions
    """

    mean = serializers.FloatField()
    median = serializers.FloatField()
    mode = serializers.FloatField()
    count = serializers.IntegerField()
    buckets = serializers.ListField()

    class Meta:
        fields = (
            "timestamp",
            "completed_count",
            "total_count",
        )


class RunTaskTrendsSerializer(serializers.Serializer):
    """
    Serializer for Recurrence's tasks trends
    """

    task_definition = ChecklistTaskDefinitionSerializer()
    response_rate = CumulativeResponseRate()
    interface_stats = serializers.SerializerMethodField(read_only=True)

    def get_interface_stats(self, instance):
        """
        get interface_stats
        """
        interface_stats = instance["interface_stats"]
        interface_type = instance["task_definition"].interface_type
        if interface_type == LINEAR_SCALE_RATING_INTERFACE:
            customization_args = instance["task_definition"].customization_args
            all_ratings = set(
                range(
                    customization_args["min_value"], customization_args["max_value"] + 1
                )
            )
            ratings = [bucket["rating"] for bucket in interface_stats.buckets]
            if len(all_ratings) != len(ratings):
                missing = [
                    {"rating": rating, "count": 0}
                    for rating in (all_ratings - set(ratings))
                ]
                interface_stats.buckets = interface_stats.combine_buckets(missing, [])
        serialized_data = NumericStats(interface_stats)
        return serialized_data.data

    class Meta:
        fields = (
            "task_definition",
            "response_rate",
            "interface_stats",
        )
        read_only_fields = fields


class ChecklistDefinitionTrendsSerializer(
    serializers.ModelSerializer
):  # pylint: disable=W0223
    """
    Serializer for ChecklistDefinition trend
    """

    response_rates = serializers.SerializerMethodField(read_only=True)
    trends = serializers.SerializerMethodField(read_only=True)

    def get_response_rates(self, instance):
        """
        build response rate data
        """
        runs = self.context["runs"]
        users = self.context["users"]
        response_rates = instance.get_response_rates(runs, users)
        serialized_data = CumulativeResponseRate(many=True, data=response_rates)
        if serialized_data.is_valid(raise_exception=True):
            return serialized_data.data
        return None

    def get_trends(self, instance):
        """
        get trends for given ChecklistDefinition
        """
        runs = self.context["runs"]
        users = self.context["users"]
        trends = instance.all_tasks_trends(runs, users)
        serialized_data = RunTaskTrendsSerializer(many=True).to_representation(trends)
        return serialized_data

    class Meta:
        model = ChecklistDefinition
        fields = (
            "id",
            "response_rates",
            "trends",
        )
        read_only_fields = fields


class ChecklistArchiveSerializer(serializers.Serializer):
    """
    Serializer for archiving/unarchiving checklist
    """

    action = serializers.ChoiceField(
        choices=ChecklistArchiveAction.choices(), required=True
    )
    ids = serializers.ListField(child=serializers.CharField(), required=True)


class ChecklistCountSerializer(serializers.Serializer):
    """Serializer for count of checklists for different list names"""

    ALL = serializers.IntegerField(required=True)
    ASSIGNED_TO_ME = serializers.IntegerField(required=True)
    TO_DO = serializers.IntegerField(required=True)


class ChecklistActiveArchiveCountSerializer(serializers.Serializer):
    """Serializer for active/archived checklists count"""

    active = ChecklistCountSerializer(required=True)
    archived = ChecklistCountSerializer(required=True)


class RunOverviewSerializer(serializers.ModelSerializer):  # pylint: disable=W0223
    """
    Serializer for Run overview
    """

    assignees = serializers.SerializerMethodField(read_only=True)
    completed_checklist_count = serializers.SerializerMethodField(read_only=True)
    total_checklist_count = serializers.SerializerMethodField(read_only=True)

    def get_assignees(self, instance):
        """
        get all assignees username and display_name
        """
        users = self.context["users"]
        checklists = instance.checklists.filter(assignee__in=users).distinct()
        team_membership_queryset = TeamMembership.objects.filter(user__in=users)
        checklists_with_team_memberships = checklists.prefetch_related(
            Prefetch(
                "assignee__teammembership_set",
                queryset=team_membership_queryset,
                to_attr="relevant_team_memberships",
            )
        )
        assignees = []
        for checklist in checklists_with_team_memberships:
            team_membership = None
            for membership in checklist.assignee.relevant_team_memberships:
                # using team_id from run instead of checklist to avoid null values
                if membership.team_id == instance.team_id:
                    team_membership = membership
                    break
            assignee_data = {
                "completed": checklist.completed,
                "display_name": checklist.assignee.display_name,
                "username": checklist.assignee.username,
                "email": checklist.assignee.email,
                "im_handle": team_membership.im_handle if team_membership else None,
            }
            assignees.append(assignee_data)
        return assignees

    def get_completed_checklist_count(self, instance):
        """
        Get completed checklist count based on users in context.
        """
        users = self.context["users"]
        return instance.checklists.filter(assignee__in=users, completed=True).count()

    def get_total_checklist_count(self, instance):
        """
        Get total checklist count based on users in context.
        """
        users = self.context["users"]
        return instance.checklists.filter(assignee__in=users).count()

    class Meta:
        model = Run
        fields = (
            "id",
            "start_date",
            "end_date",
            "team_name",
            "completed_checklist_count",
            "total_checklist_count",
            "assignees",
        )
        read_only_fields = fields


class ChecklistTaskResponseSerializer(serializers.Serializer):
    """
    Serializer for tasks.
    """

    id = serializers.CharField(read_only=True)
    assignee_name = serializers.CharField(
        read_only=True, source="checklist__assignee__display_name"
    )
    assignee_username = serializers.CharField(
        read_only=True, source="checklist__assignee__username"
    )
    task_label = serializers.CharField(read_only=True, source="definition__label")
    definition_id = serializers.CharField(read_only=True, source="definition__id")
    response = serializers.SerializerMethodField(read_only=True)
    interface_type = serializers.CharField(
        read_only=True, source="definition__interface_type"
    )
    start_date = serializers.SerializerMethodField(read_only=True)
    end_date = serializers.SerializerMethodField(read_only=True)

    def get_response(self, obj):
        """
        Get response display string
        """
        return response_repr(
            obj["response"],
            obj["definition__interface_type"],
            obj["definition__customization_args"],
        )

    def get_start_date(self, obj):
        """
        Get date from start_date (datetime) obj
        """
        return obj["checklist__run__start_date"].date()

    def get_end_date(self, obj):
        """
        Get date from end_date (datetime) obj
        """
        return obj["checklist__run__end_date"].date()


# pylint: disable=abstract-method
class SprintChecklistSerializer(serializers.Serializer):
    """
    Serialize the incoming data sent by Sprints app.
    """

    board_id = serializers.IntegerField(help_text="", min_value=1)
    cell = serializers.CharField()
    sprint_number = serializers.IntegerField(min_value=1)
    sprint_name = serializers.CharField()
    participants = serializers.DictField()
    event_name = serializers.CharField()
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()

    def validate_event_name(self, value: str) -> str:
        """
        Check if the event name is known by us.

        In case the event is not known, raise a validation error, as we cannot
        handle the request due to invalid payload.
        """

        if not EventRecurrenceMapping.objects.filter(name__iexact=value).exists():
            raise serializers.ValidationError(f'Event "{value}" is not known.')

        return value
