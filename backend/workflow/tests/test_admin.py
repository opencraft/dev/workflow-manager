"""
Tests for Workflow admin functionality.
"""

import pytest
from dateutil.relativedelta import relativedelta
from django.core import mail
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from profiles.tests.factories import TeamFactory, UserFactory
from workflow.models import Run
from workflow.tests.factories import ChecklistDefinitionFactory, RecurrenceFactory
from workflow.tests.utils import create_team_and_checklist_definition

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("api_client"),
]


def test_create_run(*, api_client):
    """
    Verify that forcibly creating a run behaves sensibly, both when no runs exist and
    when one does.
    """
    # Setup.
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team,
        checklist_definition=checklist_definition,
        start_date=timezone.now() + relativedelta(days=5),
    )

    # Action
    url = reverse("admin:workflow_recurrence_changelist")
    response = client.post(
        url,
        {
            "action": "create_next_run",
            "_selected_action": recurrence.id,
        },
        follow=True,
    )
    assert response.status_code == status.HTTP_200_OK
    assert b"Run created for Recurrence ID" in response.content
    assert Run.objects.all().count() == 1
    run = Run.objects.first()
    assert run.start_date == recurrence.start_date
    # Create second run, which should be started at the previous run's end date.
    response = client.post(
        url,
        {
            "action": "create_next_run",
            "_selected_action": recurrence.id,
        },
        follow=True,
    )
    assert b"Run created for Recurrence ID" in response.content
    assert Run.objects.all().count() == 2
    next_run = Run.objects.order_by("-end_date").first()
    assert run.end_date == next_run.start_date


def test_create_run_no_previous_no_start_date(*, api_client):
    """
    Verify that forcibly creating a run behaves sensibly if this is the first run
    and a start_date is set.
    """
    # Setup.
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition, start_date=None
    )

    # Action
    url = reverse("admin:workflow_recurrence_changelist")
    response = client.post(
        url,
        {
            "action": "create_next_run",
            "_selected_action": recurrence.id,
        },
        follow=True,
    )
    assert response.status_code == status.HTTP_200_OK
    assert b"Run created for Recurrence ID" in response.content
    assert Run.objects.all().count() == 1
    run = Run.objects.first()
    now = timezone.now()
    assert (
        (now - relativedelta(days=1)) < run.start_date < (now + relativedelta(days=1))
    )


def test_create_run_empty_checklists(*, api_client):
    """
    Verify that attempting to create a run when no valid checklists would be created
    returns a sensible info message.
    """
    # Setup.
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    team = TeamFactory()
    team.members.set([UserFactory()])
    checklist_definition = ChecklistDefinitionFactory()
    recurrence = RecurrenceFactory.create(
        team=team,
        checklist_definition=checklist_definition,
    )

    # Action
    url = reverse("admin:workflow_recurrence_changelist")
    response = client.post(
        url,
        {
            "action": "create_next_run",
            "_selected_action": recurrence.id,
        },
        follow=True,
    )

    # Verification
    assert response.status_code == status.HTTP_200_OK
    assert b"Skipped Recurrence ID" in response.content
    assert Run.objects.all().count() == 0


def assert_sent_run_emails(client, run, team):
    """
    Assert that we can forcibly send emails.
    """
    assert len(mail.outbox) == 0

    # Action
    url = reverse("admin:workflow_run_changelist")
    response = client.post(
        url,
        {
            "action": "send_email_reminders",
            "_selected_action": run.id,
        },
        follow=True,
    )

    # Verify
    assert response.status_code == status.HTTP_200_OK
    assert b"Sent email reminders for " in response.content
    assert mail.outbox
    assert len(mail.outbox) == team.members.count()


def test_send_email_reminders(*, api_client):
    """
    Verify that sending email reminders forcibly works.
    """
    # Setup.
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition, start_date=None
    )
    run = recurrence.create_run()
    assert_sent_run_emails(client, run, team)


def test_send_email_reminders_past_due(*, api_client):
    """
    Verify that sending email reminders forcibly works.
    """
    # Setup.
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition, start_date=None
    )
    run = recurrence.create_run(start_date=timezone.now() - relativedelta(years=1))
    assert run.due_date < timezone.now().date()

    assert_sent_run_emails(client, run, team)


def test_checklist_definition_admin(*, api_client):
    """
    Test that we can load up our pretty custom checklist definition model admin
    """
    # Setup
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    checklist_definition = ChecklistDefinitionFactory()

    # Action
    url = reverse(
        "admin:workflow_checklistdefinition_change", args=[checklist_definition.id]
    )
    response = client.get(url)

    # Verification
    assert response.status_code == status.HTTP_200_OK


def test_recurrence_admin(*, api_client):
    """
    Test that we can load up our pretty custom recurrence model admin
    """
    # Setup
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    team = TeamFactory()
    checklist_definition = ChecklistDefinitionFactory()
    recurrence = RecurrenceFactory.create(
        team=team,
        checklist_definition=checklist_definition,
    )

    # Action
    url = reverse("admin:workflow_recurrence_change", args=[recurrence.id])
    response = client.get(url)

    # Verification
    assert response.status_code == status.HTTP_200_OK


def test_send_email_reminders_shows_message_when_all_checklists_archived(*, api_client):
    """
    Test that sending email reminders forcibly shows appropriate message
    when all checklists are archived.
    """
    # Setup
    client = api_client()
    admin = UserFactory(is_staff=True, is_superuser=True)
    client.force_login(admin)
    (
        team,
        checklist_definition,
        _,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = recurrence.create_run()
    run.checklists.update(is_archived=True)  # Archive all checklists

    # Action
    url = reverse("admin:workflow_run_changelist")
    response = client.post(
        url,
        {
            "action": "send_email_reminders",
            "_selected_action": run.id,
        },
        follow=True,
    )

    # Verify
    assert response.status_code == status.HTTP_200_OK
    messages = list(response.context["messages"])
    assert len(messages) == 1
    expected_message = (
        f"Email reminders skipped for {run}: "
        "All the checklists in this Run are archived."
    )
    assert str(messages[0]) == expected_message
