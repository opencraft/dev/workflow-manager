# Developing Listaflow

## Requirements

- [docker and docker compose](https://docs.docker.com/engine/install/)

## Source code

Clone this repo with:

```
$ git clone git@gitlab.com:opencraft/dev/listaflow.git
$ cd listaflow
```

## Initial setup

Running `make` without any command does the initial scaffolding set-up
with docker containers for the backend running in the background and
frontend served to localhost.

```
$ make
```

This will also seed data to the database with an admin, users and teams.

For Admin user login with `admin@example.com`. The password, as well as the password for all other generated users, will be `listaflow`.

## Running

To run or attach to running docker containers:

```
$ make run
```

## Logs

The logs can be tailed through:

```
$ make logs
```

## Formatting and linting

To ensure code is formatted correctly run:

```
$ make format
```

To lint code:

```
$ make quality
```

## Tests

All quality checks and tests can be run with:

```
$ make qa
```

Or, just run tests with:

```
$ make test
```
