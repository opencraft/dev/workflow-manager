# Workflow Manager and Sprint Planning Checklist Automation - Project Split Proposal

## Introduction

When planning the sprint planning checklist automation project,[^sprint-planning-checklist-automation-project-planning] it was agreed up on that there will be two different fronts to work on, maintaining and further automation the existing Monday.com solution and working on the open source workflow manager.

The work for the open source workflow manager has started.[^proposition]

## Problem

As mentioned earlier, the open source workflow manager has become its own project. Although both the sprint planning checklist automation project and the open source workflow manager project have been created with the same goal in mind, which is improving the sprint planning process and other processes, they are currently heading in different directions.

The sprint planning checklist automation project's next phase requires maintaining the solution that was created using [monday.com][monday-com] and further automating some sprint planning checklist tasks.[^sprint-planning-checklist-automation-further-automation-discovery] Meanwhile, the open source workflow manager requires working on a new platform that would be manage different workflows, and not just the sprint planning workflow.

In addition, Sprints v2 plans to automate a lot of the asynchronous sprint planning process.[^sprints-v2-discovery] A lot of the planned automation would result in changes to the sprint planning checklist, which would either introduce new automation capabilities or render some other planned automation useless.

## Suggested Solution

Splitting the project to two different cells would be beneficial because it would allow distributing each cell's efforts towards each direction, instead of stretching the single cell's efforts in both directions.

However, even after splitting the project, it is important to maintain good communication regarding the updates of the different projects, sprint planning checklist automation, workflow manager, and sprints v2, to provide strong cooperation between the projects.

In addition, any requests or suggestions made to each of the projects from a different project should be treated as a client request, with the priority to creating a generic solution that wouldn't be specific to a single client. Such solutions would be discussed in the future through feature or project discoveries.

Meanwhile, the sprint planning checklist automation project's workflow would be slightly extended to include implementing the sprint planning checklist as a workflow on the workflow manager once the development is at a state which would meet the necessary requirements.

<!-- FOOT NOTES -->

[^sprint-planning-checklist-automation-project-planning]: [Discovery - Sprint Planning Checklist Automation - Phase and Ticket Planning][sprint-planning-checklist-automation-google-docs-discovery].
[^proposition]: [Workflow Manager - Requirements, Proposition and Initial Steps][proposition-gitlab-issue]
[^sprint-planning-checklist-automation-further-automation-discovery]: The next discovery planning for Phase 3 of the sprint planning checklist automation project is [“Identifying Things that can be Further Automated”][sprint-planning-checklist-automation-further-automation-google-docs-header].
[^sprints-v2-discovery]: [Discovery - Sprints v2][sprints-v2-discovery-gitlab].

<!-- LINKS -->

[sprint-planning-checklist-automation-google-docs-discovery]: https://docs.google.com/document/d/1SpAtXahKTFR_8bpJMk2kMFgM1v-QnbcJ0_gSeDa0LiY/edit?usp=sharing
[proposition-gitlab-issue]: https://gitlab.com/opencraft/dev/workflow-manager/-/issues/1
[sprint-planning-checklist-automation-further-automation-google-docs-header]: https://docs.google.com/document/d/1SpAtXahKTFR_8bpJMk2kMFgM1v-QnbcJ0_gSeDa0LiY/edit#heading=h.toepyryl2knz
[monday-com]: https://monday.com
[sprints-v2-discovery-gitlab]: https://gitlab.com/opencraft/dev/sprints/-/merge_requests/1
