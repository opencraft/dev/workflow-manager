# Listaflow Frontend

Listaflow's frontend code is built with [Vite](https://vitejs.dev/) and for development is run via Vite's live server in a node container. It's launched as part of the standard services in `make up`.

If you need to launch just the frontend, you can do so with:

```
make run.frontend
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You can test frontend with:

```
make test.frontend
```

You can also build frontend assets with:

```
make build.frontend
```
