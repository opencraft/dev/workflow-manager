import { useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { dateString, isFirstOlderThanSecond } from "../utils/helpers";
import { ChecklistAction } from "./ChecklistAction";
import { TaskListReview } from "../types/Task";
import { format, parseISO } from "date-fns";
import { REPORTS_COMPARE, USER_CHECKLISTS } from "../constants/urls";
import { useTranslation } from "react-i18next";

declare interface ColSizes {
  xs: number[];
  sm: number[];
  md: number[];
  lg: number[];
}

declare interface ChecklistTableRowArgs {
  checklist: TaskListReview;
  currentUsername: string;
  isArchived: boolean;
  selectedChecklist: Set<string>;
  setChecklistSelectState: (id: string, value: boolean) => void;
  onSingleArchiveUnarchive: (id: string, isArchived: boolean) => void;
  colSizes: ColSizes;
}

export const ChecklistTableRow = ({
  checklist,
  currentUsername,
  isArchived,
  selectedChecklist,
  setChecklistSelectState,
  onSingleArchiveUnarchive,
  colSizes,
}: ChecklistTableRowArgs) => {
  const isCurrentUserAssignee = checklist.assignee.username === currentUsername;
  const isClickable = (isCurrentUserAssignee || checklist.run) && !isArchived;
  const { t } = useTranslation();

  const [isHovering, setIsHovering] = useState(false);

  const getTaskReportLink = (taskList: TaskListReview): string => {
    if (!(taskList.run && taskList.run.recurrence)) {
      return "";
    }
    const reportQueryParams = new URLSearchParams({
      checklistDefinition: taskList.run.recurrence.checklist_definition,
      team_ids: taskList.run.recurrence.team,
      start_date: format(parseISO(taskList.run.start_date), "yyyy-MM-dd"),
      end_date: format(parseISO(taskList.run.end_date), "yyyy-MM-dd"),
    });
    return `${REPORTS_COMPARE}?${reportQueryParams.toString()}`;
  };

  const singleUnarchive = () => {
    onSingleArchiveUnarchive(checklist.id, false);
  };

  const detailLink = `${USER_CHECKLISTS}/${checklist.id}/`;
  const isSelected = selectedChecklist.has(checklist.id);
  const anySelected = selectedChecklist.size > 0;

  return (
    <Row>
      <Col
        xs="auto"
        className={`py-4 pe-0 list-checkbox ${anySelected ? "" : "hidden"}`}
      >
        <Form.Check.Input
          type="checkbox"
          className="sm interaction"
          id={`run-${checklist.id}-checkbox`}
          onChange={() => {
            setChecklistSelectState(checklist.id, !isSelected);
          }}
          checked={isSelected}
        />
      </Col>
      <Col>
        <Row
          key={checklist.id}
          className="item mb-2 rounded py-4 me-0 ms-0 fs-sm run-listing-item"
          onMouseLeave={() => setIsHovering(false)}
        >
          {isArchived && (
            <div
              aria-hidden={true}
              className={
                "unarchive-overlay rounded" + (isHovering ? " activated" : "")
              }
              onClick={singleUnarchive}
            >
              <div className={"unarchive-label"}>
                {t("checklists.table.row.clickToUnarchive")}
              </div>
            </div>
          )}
          {isClickable && (
            <Link
              to={detailLink}
              state={{ fromMyLists: true }}
              className={"link-overlay rounded"}
            >
              <span className={"d-none"}>{checklist.name}</span>
            </Link>
          )}
          <Col
            xs={colSizes.xs[0]}
            sm={colSizes.sm[0]}
            md={colSizes.md[0]}
            lg={colSizes.lg[0]}
            onMouseEnter={() => setIsHovering(false)}
          >
            <Form.Check.Label
              title={checklist.name}
              htmlFor={`run-${checklist.id}-checkbox`}
              className="fw-bold d-inline-block text-truncate w-50 w-xl-75 white interaction"
            >
              <bdi>{checklist.name}</bdi>
            </Form.Check.Label>
          </Col>
          <Col
            className="d-none d-md-block"
            xs={colSizes.xs[1]}
            sm={colSizes.sm[1]}
            md={colSizes.md[1]}
            lg={colSizes.lg[1]}
            onMouseEnter={() => setIsHovering(true)}
          >
            <bdi>{checklist.run?.team_name}</bdi>
          </Col>
          {!isArchived && (
            <Col
              xs={colSizes.xs[2]}
              sm={colSizes.sm[2]}
              md={colSizes.md[2]}
              lg={colSizes.lg[2]}
            >
              {isCurrentUserAssignee && (
                <div
                  className={`noselect checklist-status ${checklist.status.toLowerCase()}`}
                >
                  <small>
                    {t(`checklistStatus.${checklist.status.toLowerCase()}`)}
                  </small>
                </div>
              )}
            </Col>
          )}
          <Col
            xs={colSizes.xs[3]}
            sm={colSizes.sm[3]}
            md={colSizes.md[3]}
            lg={colSizes.lg[3]}
            onMouseEnter={() => setIsHovering(true)}
          >
            <Row>
              <Col
                className="d-none d-sm-block pe-0"
                xs={0}
                sm={9}
                md={10}
                lg={9}
              >
                <span
                  className={`${!checklist.completed && isFirstOlderThanSecond(checklist.run?.due_date, new Date()) ? "text-warning" : ""} noselect`}
                >
                  {dateString(checklist.run?.due_date)}
                </span>
              </Col>
              <Col
                className="p-0"
                xs={12}
                sm={3}
                md={2}
                lg={3}
                onMouseEnter={() => setIsHovering(false)}
              >
                <ChecklistAction
                  onArchiveUnarchive={onSingleArchiveUnarchive}
                  checklist={checklist}
                  isOwnedByCurrentUser={isCurrentUserAssignee}
                  taskReportLink={getTaskReportLink(checklist)}
                  fromMyLists={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
