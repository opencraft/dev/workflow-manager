import { useSingle } from "@opencraft/providence-redux/hooks";
import ReactCanvasConfetti from "react-canvas-confetti";
import { useEffect, useRef } from "react";
import { TOnInitComponentFn } from "react-canvas-confetti/src/types";
import { TCanvasConfettiInstance } from "react-canvas-confetti/src/types/normalization";

export declare interface ConfettiState {
  fire: boolean | number;
}

export const Confetti = () => {
  const confettiState = useSingle<ConfettiState>("confettiState", {
    endpoint: "#",
    x: { fire: false },
  });
  const controller = useRef<TCanvasConfettiInstance>();
  const onInitHandler: TOnInitComponentFn = ({ confetti }) => {
    controller.current = confetti;
  };
  const options = {
    origin: { y: 0.35 },
    spread: 150,
    startVelocity: 80,
    particleCount: 100,
    ticks: 100,
    gravity: 1,
  };
  useEffect(() => {
    if (!confettiState.p.fire.model) {
      return;
    }
    controller.current && controller.current(options);
  }, [confettiState.p.fire.model]);
  return <ReactCanvasConfetti onInit={onInitHandler} className="confetti" />;
};
