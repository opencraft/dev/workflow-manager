import { TaskListReview } from "../types/Task";
import { Paginated } from "./Paginated";
import Container from "react-bootstrap/Container";
import { Col, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import { RunChecklistTableRow } from "./RunChecklistTableRow";
import { Single } from "@opencraft/providence-redux/components/Single";

declare interface RunChecklistsTableArgs {
  controller: ListController<TaskListReview>;
  currentUsername: string;
}

export const RunChecklistsTable = ({
  controller,
  currentUsername,
}: RunChecklistsTableArgs) => {
  const { t } = useTranslation();
  const headingColSizes = {
    xs: [8, 0, 4, 0],
    sm: [5, 4, 2, 0],
    md: [4, 3, 2, 3],
    lg: [3, 3, 3, 3],
  };
  const colSizes = {
    xs: [8, 0, 3, 1],
    sm: [5, 4, 2, 1],
    md: [4, 3, 2, 3],
    lg: [3, 3, 3, 3],
  };
  const isAssigneeCurrentUser = (singleController) =>
    /* Returns an integer for simpler use in sort function. */
    singleController.x!.assignee.username === currentUsername ? 0 : 1;
  return (
    <Paginated hideTop controller={controller}>
      <Container className="checklist-table">
        {controller.list.length > 0 ? (
          <>
            <Row className="position-sticky top-n1 me-0 ms-0 py-4 fs-sm heading z-index-sticky">
              <Col
                xs={headingColSizes.xs[0]}
                sm={headingColSizes.sm[0]}
                md={headingColSizes.md[0]}
                lg={headingColSizes.lg[0]}
              >
                {t("runDetail.checklists.table.heading.assignee")}
              </Col>
              <Col
                className="d-none d-sm-block"
                xs={headingColSizes.xs[1]}
                sm={headingColSizes.sm[1]}
                md={headingColSizes.md[1]}
                lg={headingColSizes.lg[1]}
              >
                {t("runDetail.checklists.table.heading.handle")}
              </Col>
              <Col
                xs={headingColSizes.xs[2]}
                sm={headingColSizes.sm[2]}
                md={headingColSizes.md[2]}
                lg={headingColSizes.lg[2]}
              >
                {t("runDetail.checklists.table.heading.status")}
              </Col>
              <Col
                className="d-none d-md-block"
                xs={headingColSizes.xs[3]}
                sm={headingColSizes.sm[3]}
                md={headingColSizes.md[3]}
                lg={headingColSizes.lg[3]}
              >
                {t("runDetail.checklists.table.heading.due")}
              </Col>
            </Row>
            {controller.list
              .sort(
                (a, b) =>
                  /* Sort checklist assigned to current user as first. */
                  isAssigneeCurrentUser(a) - isAssigneeCurrentUser(b),
              )
              .map((singleController) => (
                <Single
                  key={singleController.x!.id}
                  controller={singleController}
                >
                  {() => (
                    <RunChecklistTableRow
                      checklist={singleController.x!}
                      currentUsername={currentUsername}
                      colSizes={colSizes}
                    />
                  )}
                </Single>
              ))}
          </>
        ) : (
          <Row className="item mb-2 rounded p-4 run-listing-item">
            <Col>{t("runDetail.checklists.table.message.noChecklists")}</Col>
          </Row>
        )}
      </Container>
    </Paginated>
  );
};
