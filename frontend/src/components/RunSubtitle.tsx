import { Run } from "../types/Task";
import { useTranslation } from "react-i18next";
import { dateString } from "../utils/helpers";

declare interface RunSubtitleArgs {
  run: Run;
}

export const RunSubtitle = ({ run }: RunSubtitleArgs) => {
  const { t } = useTranslation();

  return (
    <span className="run-subtitle">
      <span>{run.team_name}</span>
      <span className="divider">|</span>
      <span>
        {t("runs.runSubtitle.dateRange", {
          startDate: dateString(run.start_date, true),
          endDate: dateString(run.end_date),
        })}
      </span>
      <span className="divider">|</span>
      <span>{dateString(run.due_date)}</span>
    </span>
  );
};
