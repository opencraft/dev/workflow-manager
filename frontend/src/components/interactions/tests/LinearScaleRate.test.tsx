import {
  fireEvent,
  render,
  screen,
  testContext,
} from "../../../utils/test-utils";
import { LinearScaleRate } from "../LinearScaleRate";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { describe, it, beforeEach, expect } from "vitest";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { getSingle } from "@opencraft/providence-redux/testHelpers";

let store: IModuleStore<any>;
let context: GlobalOptions;
let controller: SingleController<InterfaceComponentProps>;

describe("LinearScaleRate component", () => {
  beforeEach(() => {
    store = createStore({});
    context = testContext();
    controller = getSingle<InterfaceComponentProps>(
      "testController",
      {
        endpoint: "#",
        x: {
          customizationArgs: {
            min_value: 1,
            max_value: 5,
            min_value_description: "Bad",
            max_value_description: "Good",
          },
          response: null,
        },
      },
      { store, context },
    ).controller;
    render(
      <LinearScaleRate
        customizationArgs={controller.x!.customizationArgs}
        response={controller.p.response}
      />,
    );
  });

  it("should render list of rating items correctly", async () => {
    const ratingItems = screen.getAllByText(/\d/i);
    expect(ratingItems.length).toEqual(5);
    expect(ratingItems.map((i) => i.textContent)).toEqual([
      "1",
      "2",
      "3",
      "4",
      "5",
    ]);
  });

  it("should render min value description with correct style", async () => {
    const minValueDescription = screen.getByText("Bad");
    expect(minValueDescription).toBeInTheDocument();
    expect(minValueDescription.className).toContain("rating-description-label");
  });

  it("should render max value description with correct style", async () => {
    const maxValueDescription = screen.getByText("Good");
    expect(maxValueDescription).toBeInTheDocument();
    expect(maxValueDescription.className).toContain("rating-description-label");
  });

  it("should update response correctly on clicking a rating item", async () => {
    const ratingItemsWithValue3 = screen.getByText("3");
    fireEvent.click(ratingItemsWithValue3);

    expect(controller.p.response.model).toStrictEqual({ rating: 3 });
  });
});
