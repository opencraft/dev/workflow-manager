import {
  customRender,
  fireEvent,
  screen,
  testContext,
} from "../../../utils/test-utils";
import { describe, it, expect, beforeEach } from "vitest";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { MultipleChoice } from "../MultipleChoice";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { getSingle } from "@opencraft/providence-redux/testHelpers";

let store: IModuleStore<any>;
let context: GlobalOptions;
let controller: SingleController<InterfaceComponentProps>;
const available_choices = ["A", "B", "C", "D", "E"];

describe("MultipleChoice component with minimum and maximum limit", () => {
  beforeEach(() => {
    store = createStore({});
    context = testContext();
    controller = getSingle<InterfaceComponentProps>(
      "testController",
      {
        endpoint: "#",
        x: {
          customizationArgs: {
            minimum_selected: 1,
            maximum_selected: 3,
            available_choices,
          },
          response: null,
        },
      },
      { store, context },
    ).controller;
    customRender(
      <MultipleChoice
        customizationArgs={controller.x!.customizationArgs}
        response={controller.p.response}
      />,
      { store, context },
    );
  });

  it("should render options correctly", async () => {
    for (const item of available_choices) {
      const input = screen.getByText(item);
      expect(input).toBeInTheDocument();
      expect(input.className).toContain("btn-pill-check");
    }
    const selectAll = screen.queryByText("multiChoice.select_all");
    expect(selectAll).not.toBeInTheDocument();
  });

  it("should render select label correctly", async () => {
    const label = screen.getByText("multiChoice.minmax");
    expect(label).toBeInTheDocument();
  });

  it("should set response correctly", async () => {
    const A = screen.getByText("A");
    fireEvent.click(A);
    expect(controller.p.response.model).toStrictEqual({
      selected_choices: ["A"],
    });

    const D = screen.getByText("D");
    fireEvent.click(D);
    expect(controller.p.response.model).toStrictEqual({
      selected_choices: ["A", "D"],
    });
  });
});

describe("MultipleChoice component with only minimum limit", () => {
  beforeEach(() => {
    store = createStore({});
    context = testContext();
    controller = getSingle<InterfaceComponentProps>(
      "testController",
      {
        endpoint: "#",
        x: {
          customizationArgs: {
            minimum_selected: 1,
            available_choices,
          },
          response: null,
        },
      },
      { store, context },
    ).controller;
    customRender(
      <MultipleChoice
        customizationArgs={controller.x!.customizationArgs}
        response={controller.p.response}
      />,
      { store, context },
    );
  });

  it("should render options correctly", async () => {
    for (const item of available_choices) {
      const input = screen.getByText(item);
      expect(input).toBeInTheDocument();
      expect(input.className).toContain("btn-pill-check");
    }
    const selectAll = screen.getByText("multiChoice.select_all");
    expect(selectAll).toBeInTheDocument();
  });

  it("should render select label correctly", async () => {
    const label = screen.getByText("multiChoice.minimumOnly");
    expect(label).toBeInTheDocument();
  });

  it("should set response correctly", async () => {
    const A = screen.getByText("A");
    fireEvent.click(A);
    expect(controller.p.response.model).toStrictEqual({
      selected_choices: ["A"],
    });

    const D = screen.getByText("D");
    fireEvent.click(D);
    expect(controller.p.response.model).toStrictEqual({
      selected_choices: ["A", "D"],
    });

    const selectAll = screen.getByText("multiChoice.select_all");
    fireEvent.click(selectAll);
    expect(controller.p.response.model).toStrictEqual({
      selected_choices: available_choices,
    });
  });
});
