import { useTranslation } from "react-i18next";
import { InterfaceComponentProps } from "../../types/Interaction";

export const LinearScaleRate = ({
  customizationArgs,
  response,
}: InterfaceComponentProps) => {
  const { t } = useTranslation();

  const num = response.model?.rating;
  const end = customizationArgs.max_value!;

  return (
    <span>
      {num !== undefined
        ? t("taskItem.outOf", { value: num, outOf: end })
        : t("taskItem.noAnswer")}
    </span>
  );
};
