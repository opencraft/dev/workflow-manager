import { useTranslation } from "react-i18next";
import { InterfaceComponentProps } from "../../types/Interaction";

export const NumericInput = ({ response }: InterfaceComponentProps) => {
  const { t } = useTranslation();
  const num = response.model?.number;

  return <span>{num !== undefined ? num : t("taskItem.noAnswer")}</span>;
};
