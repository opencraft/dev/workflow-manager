import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { Checkbox } from "./Checkbox";
import { LinearScaleRate } from "./LinearScaleRate";
import { MarkdownTextarea } from "./MarkdownTextarea";
import { InterfaceComponentProps } from "../../types/Interaction";
import { NumericInput } from "./NumericInput";
import { MultipleChoice } from "./MultipleChoice";

const INTERFACE_NAME_TO_COMPONENT: {
  [interfaceName: string]: (a: InterfaceComponentProps) => JSX.Element;
} = {
  linear_scale_rating: LinearScaleRate,
  markdown_textarea: MarkdownTextarea,
  numeric: NumericInput,
  multi_choice: MultipleChoice,
  checkbox: Checkbox,
};

declare interface ReadOnlyItemProps {
  interfaceType: string;
  customizationArgs: any;
  response: any;
}

export const ReadOnlyItem = ({
  interfaceType,
  customizationArgs,
  response,
}: ReadOnlyItemProps) => {
  const InterfaceComponent = INTERFACE_NAME_TO_COMPONENT[interfaceType];

  return (
    <InterfaceComponent
      customizationArgs={customizationArgs}
      response={response}
    ></InterfaceComponent>
  );
};
