import { LinearScaleRate } from "../LinearScaleRate";
import { render } from "../../../utils/test-utils";
import { describe, it, expect } from "vitest";

describe("LinearScaleRate.tsx", () => {
  const customizationArgs = {
    max_value: 5,
  };

  it("Renders a rating response.", () => {
    const result = render(
      <LinearScaleRate
        customizationArgs={customizationArgs}
        response={{ model: { rating: 2 } }}
      />,
    );
    expect(result.queryByText("taskItem.outOf")).toBeInTheDocument();
  });

  it("Renders an empty rating response.", () => {
    const result = render(
      <LinearScaleRate
        customizationArgs={customizationArgs}
        response={{ model: {} }}
      />,
    );
    expect(result.queryByText("taskItem.noAnswer")).toBeInTheDocument();
  });
});
