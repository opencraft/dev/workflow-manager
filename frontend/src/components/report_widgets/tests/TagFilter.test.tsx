import {
  customRender,
  fireEvent,
  screen,
  testContext,
} from "../../../utils/test-utils";
import { vi, beforeEach, describe, it, expect } from "vitest";
import { TagFilter } from "../TagFilter";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { defaultContextValues } from "@opencraft/providence-redux/context";
import { getList } from "@opencraft/providence-redux/testHelpers";
import { Tag } from "../../../types/Definition";

let store: IModuleStore<any>;
let context: GlobalOptions;
let selectedTags = ["tag1"];
const onChangeTestFunc = vi.fn();

describe("TagFilter component", () => {
  beforeEach(() => {
    store = createStore({});
    context = testContext();
    const { controller } = getList<Tag>(
      ["dummy-cd", "related-tags"],
      { endpoint: "#" },
      { store, context },
    );
    const ui = (
      <TagFilter
        selectedTags={selectedTags}
        checklistDefinition={"dummy-cd"}
        onChange={onChangeTestFunc}
      />
    );
    customRender(ui, { context, store });
    controller.makeReady([
      { id: 1, name: "tag1" },
      { id: 2, name: "tag2" },
    ]);
  });

  it("should render tags correctly", () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag1 = screen.getByText("tag1");
    const tag2 = screen.getByText("tag2");
    expect(tag1).toBeInTheDocument();
    expect(tag2).toBeInTheDocument();
  });

  it("should render selected tags as checked", () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag1Checkbox = screen.getByText("tag1")
      ?.previousElementSibling as HTMLElement;
    expect(tag1Checkbox).toHaveAttribute("checked");
    const tag2Checkbox = screen.getByText("tag2")
      ?.previousElementSibling as HTMLElement;
    expect(tag2Checkbox).not.toHaveAttribute("checked");
  });

  it("should trigger change function on changing tag selection", () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag2Checkbox = screen.getByText("tag2")
      ?.previousElementSibling as HTMLElement;
    fireEvent.click(tag2Checkbox);
    expect(onChangeTestFunc).toHaveBeenCalled();
  });
});
