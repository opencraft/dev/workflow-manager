import { act } from "@testing-library/react";
import { ChecklistStatus } from "../../types/Task";
import { ChecklistAction } from "../ChecklistAction";
import { expect, vi, describe, it } from "vitest";
import { customRender, mockAxios } from "../../utils/test-utils";

describe("ChecklistAction.tsx", () => {
  const user = {
    id: 1,
    username: "testuser",
    email: "test@example.com",
  };
  const checklist = {
    id: "test_checklist_id",
    name: "Test Checklist",
    body: "test body",
    assignee: user,
    created_on: "2024-03-07T12:00:00Z",
    completed: false,
    completed_on: null,
    run: null,
    completed_task_count: 5,
    total_task_count: 10,
    status: ChecklistStatus.IN_PROGRESS,
    is_archived: false,
  };

  it("Shows task report link", async () => {
    let result;

    result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={false}
        onArchiveUnarchive={null}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await result.findByText("checklists.action.showReport");

    result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={true}
        onArchiveUnarchive={null}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await result.findByText("checklists.action.showReport");
  });

  it("Shows archive option on owned checklist", async () => {
    const result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={true}
        onArchiveUnarchive={vi.fn()}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await result.findByText("checklists.action.archive");
  });

  it("Shows unarchive option on archived checklist", async () => {
    const result = customRender(
      <ChecklistAction
        checklist={{ ...checklist, is_archived: true }}
        isOwnedByCurrentUser={true}
        onArchiveUnarchive={vi.fn()}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await result.findByText("checklists.action.unarchive");
  });

  it("Does not show archive option on non-owned checklist", async () => {
    const result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={false}
        onArchiveUnarchive={vi.fn()}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await expect(
      result.findByText("checklists.action.archive"),
    ).rejects.toThrow();
  });

  it("Does not show archive option with no archive function", async () => {
    const result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={true}
        onArchiveUnarchive={null}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    await expect(
      result.findByText("checklists.action.archive"),
    ).rejects.toThrow();
  });

  it("Calls the archive-unarchive function", async () => {
    const onArchiveUnarchive = vi.fn();
    const result = customRender(
      <ChecklistAction
        checklist={checklist}
        isOwnedByCurrentUser={true}
        onArchiveUnarchive={onArchiveUnarchive}
        taskReportLink="/task_report_test"
        fromMyLists={false}
      />,
    );
    act(() => result.getAllByRole("button")[0].click());
    const button = await result.findByText("checklists.action.archive");
    act(() => button.click());
    expect(onArchiveUnarchive).toHaveBeenCalled();
  });
});
