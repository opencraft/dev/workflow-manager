import { customRender } from "../../utils/test-utils";
import { ChecklistStatus, TaskList } from "../../types/Task";
import { RunChecklistTableRow } from "../RunChecklistTableRow";
import * as ChecklistAction from "../ChecklistAction";
import { expect, vi, describe, it, beforeEach, afterEach } from "vitest";

vi.mock("../ChecklistAction", () => ({
  ChecklistAction: () => <div data-testid="checklist-action" />,
}));

describe("RunChecklistTableRow.tsx", () => {
  const user = {
    id: 1,
    username: "testuser",
    email: "test@example.com",
  };
  const run = {
    id: "test_run_id",
    start_date: "2024-03-07T12:00:00Z",
    end_date: "2024-03-08T12:00:00Z",
    due_date: "2024-03-09",
    name: "Test Run",
    team_name: "Test Team",
    is_archived: false,
    completed_checklist_count: 0,
    total_checklist_count: 1,
    assignees: [
      {
        ...user,
        completed: false,
        display_name: "Test User",
      },
    ],
    recurrence: {
      id: "test_recurrence_id",
      periodic_task: 3,
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
      checklist_definition: "test_checklist_definition_id",
    },
  };
  const checklist = {
    id: "test_checklist_id",
    name: "Test Checklist",
    body: "test body",
    assignee: user,
    created_on: "2024-03-07T12:00:00Z",
    completed: false,
    completed_on: null,
    run: run,
    completed_task_count: 5,
    total_task_count: 10,
    status: ChecklistStatus.IN_PROGRESS,
    is_archived: false,
  };
  const colSizes = {
    xs: [8, 0, 3, 1],
    sm: [5, 4, 2, 1],
    md: [4, 3, 2, 3],
    lg: [3, 3, 3, 3],
  };

  let checklistActionSpy;

  beforeEach(async () => {
    checklistActionSpy = vi.spyOn(ChecklistAction, "ChecklistAction");
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it("Displays run checklist information", () => {
    const result = customRender(
      <RunChecklistTableRow
        checklist={checklist}
        currentUsername="user"
        colSizes={colSizes}
      />,
    );
    expect(checklistActionSpy).toHaveBeenCalledWith(
      {
        checklist: checklist,
        isOwnedByCurrentUser: false,
        onArchiveUnarchive: null,
        taskReportLink:
          "/reports/compare?checklistDefinition=test_checklist_definition_id" +
          "&team_ids=test_team_id&start_date=2024-03-07&end_date=2024-03-08",
        fromMyLists: false,
      },
      {},
    );
  });

  it("Displays options for checklist owned by current user", () => {
    const result = customRender(
      <RunChecklistTableRow
        checklist={checklist}
        currentUsername={user.username}
        colSizes={colSizes}
      />,
    );
    expect(result.getByTestId("checklist-action")).toBeInTheDocument();
    expect(checklistActionSpy).toHaveBeenCalledWith(
      {
        checklist: checklist,
        isOwnedByCurrentUser: true,
        onArchiveUnarchive: null,
        taskReportLink:
          "/reports/compare?checklistDefinition=test_checklist_definition_id" +
          "&team_ids=test_team_id&start_date=2024-03-07&end_date=2024-03-08",
        fromMyLists: false,
      },
      {},
    );
  });
});
