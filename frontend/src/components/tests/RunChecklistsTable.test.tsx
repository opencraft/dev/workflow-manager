import { createStore, IModuleStore } from "redux-dynamic-modules";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { getList } from "@opencraft/providence-redux/testHelpers";
import { customRender, testContext } from "../../utils/test-utils";
import { TaskListReview, ChecklistStatus } from "../../types/Task";
import { RunChecklistsTable } from "../RunChecklistsTable";
import { expect, vi, describe, it, beforeEach } from "vitest";

vi.mock("../RunChecklistTableRow", () => ({
  RunChecklistTableRow: () => <div data-testid="run-checklist-table-row" />,
}));

let store: IModuleStore<any>;
let context: GlobalOptions;

describe("RunChecklistsTable.tsx", () => {
  const checklistData = {
    id: "test_checklist_id",
    name: "Test Checklist",
    body: "test body",
    assignee: null,
    created_on: "2024-03-07T12:00:00Z",
    completed: false,
    completed_on: null,
    run: null,
    completed_task_count: 5,
    total_task_count: 10,
    status: ChecklistStatus.IN_PROGRESS,
    is_archived: false,
  };

  beforeEach(() => {
    store = createStore({});
    context = testContext();
  });

  it("Displays one row for each checklist", async () => {
    const { controller } = getList<TaskListReview>(
      "testList",
      { endpoint: "#" },
      { store, context },
    );
    await controller.makeReady([
      {
        ...checklistData,
        id: "test_run_checklist_1",
        assignee: { id: 1, username: "user1", email: "user1@example.com" },
      },
      {
        ...checklistData,
        id: "test_run_checklist_2",
        assignee: { id: 2, username: "user2", email: "user2@example.com" },
      },
    ]);
    const result = customRender(
      <RunChecklistsTable controller={controller} currentUsername="" />,
    );
    expect(result.getAllByTestId("run-checklist-table-row")).toHaveLength(2);
  });

  it("Displays a message when there are no checklists", async () => {
    const { controller } = getList<TaskListReview>(
      "testList",
      { endpoint: "#" },
      { store, context },
    );
    await controller.makeReady([]);
    const result = customRender(
      <RunChecklistsTable controller={controller} currentUsername="" />,
    );
    expect(
      result.getByText("runDetail.checklists.table.message.noChecklists"),
    ).toBeInTheDocument();
  });
});
