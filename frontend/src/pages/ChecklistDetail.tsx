import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { useList, useSingle } from "@opencraft/providence-redux/hooks";
import { ChecklistStatus, Task, TaskList } from "../types/Task";
import { DisplayUser } from "../types/User";
import { Paginated } from "../components/Paginated";
import { ReadOnlyTaskItem, TaskItem } from "../components/TaskItem";
import {
  ReadOnlySubsection,
  Subsection,
  SubsectionTask,
} from "../components/Subsection";
import { CHECKLIST_LIST_API } from "../constants/api-urls";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Offcanvas from "react-bootstrap/Offcanvas";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { Link, useParams, useLocation } from "react-router-dom";
import { Body } from "../components/Body";
import { Title } from "../components/Title";
import { CustomAlert } from "../components/Alerts";
import { LoadSection } from "../components/LoadSection";
import { ProgressState } from "../components/ChecklistProgress";
import useAuth from "../hooks/useAuth";
import { Trans, useTranslation } from "react-i18next";
import { dateString, daysUntil } from "../utils/helpers";
import { ConfettiState } from "../components/Confetti";
import { Single } from "@opencraft/providence-redux/components/Single";
import { useSelector } from "react-redux";
import DoubleChevronIcon from "../assets/icons/double-chevron.svg?react";
import ShareListIcon from "../assets/icons/share-list.svg?react";
import { MiniAvatar } from "../components/MiniAvatar";
import useNotification from "../hooks/useNotification";
import * as urls from "../constants/urls";

type ChecklistDetailRouteParams = {
  checklistId: string;
};

declare interface TaskWithSubtasksItem {
  taskController: SingleController<Task>;
  readOnly: boolean;
  subtasks: SingleController<Task>[];
}

declare interface TaskMap {
  [key: string]: SingleController<Task>[];
}

declare interface ChecklistState {
  totalTasks: number;
  completedTasks: number;
  requiredCompletedTasks: number;
  requiredTotalTasks: number;
  incompleteRequiredTasks: SingleController<Task>[];
  tasksWithInteraction: string[];
  failed: boolean;
  taskMap: TaskMap;
  rootTasks: SingleController<Task>[];
}

const ensureMapId = (taskMap: TaskMap, id: string) => {
  if (!taskMap[id]) {
    taskMap[id] = [];
  }
};

const performTallies = (
  stats: ChecklistState,
  tasks: SingleController<Task>[],
) => {
  tasks.forEach((task: SingleController<Task>) => {
    ensureMapId(stats.taskMap, task.x.id);
    if (task.x.parent) {
      ensureMapId(stats.taskMap, task.x.parent);
      stats.taskMap[task.x.parent].push(task);
    } else {
      stats.rootTasks.push(task);
    }
    if (task.x.interface_type !== "checkbox") {
      stats.tasksWithInteraction.push(task.x.id);
    }
    updateChecklistStatePerTask(stats, task);
  });
};

function updateChecklistStatePerTask(
  stats: ChecklistState,
  task: SingleController<Task>,
) {
  stats.totalTasks += 1;
  if (task.x.required) {
    stats.requiredTotalTasks += 1;
    if (task.x.completed) {
      stats.requiredCompletedTasks += 1;
    } else {
      stats.incompleteRequiredTasks.push(task);
    }
    if (task.p.response.errors.length && !stats.failed) {
      stats.failed = true;
    }
  }
  if (task.x.completed) {
    stats.completedTasks += 1;
  }
}

const TaskWithSubtasks = ({
  taskController,
  readOnly,
  subtasks,
}: TaskWithSubtasksItem) => {
  const TaskItemType = readOnly ? ReadOnlyTaskItem : TaskItem;
  const SubsectionType = readOnly ? ReadOnlySubsection : Subsection;

  const taskIsSubsection = taskController.x!.interface_type === "subsection";

  if (!taskIsSubsection) {
    return <TaskItemType controller={taskController} />;
  }

  const subsectionComplete =
    (taskController.x!.required
      ? subtasks.filter((sc) => sc.x!.required).filter((sc) => !sc.x!.completed)
          .length
      : subtasks.filter((sc) => !sc.x!.completed).length) === 0;

  return (
    <div className="checklist-item">
      <SubsectionType
        controller={taskController as SingleController<SubsectionTask>}
        isCompleted={subsectionComplete}
      >
        {subtasks.map((cc) => (
          <Single key={cc.x!.id} controller={cc}>
            {() => <TaskItemType controller={cc} />}
          </Single>
        ))}
      </SubsectionType>
    </div>
  );
};

const emptyChecklistStats: () => ChecklistState = () => ({
  totalTasks: 0,
  completedTasks: 0,
  incompleteRequiredTasks: [],
  requiredCompletedTasks: 0,
  requiredTotalTasks: 0,
  tasksWithInteraction: [],
  failed: false,
  taskMap: {},
  rootTasks: [],
});

declare interface ForceSubmitAlertProps {
  setShowForceSubmitAlert: (val: boolean) => void;
  submitChecklist: (val: boolean) => void;
  showForceSubmitAlert: boolean;
}

const ForceSubmitAlert = ({
  setShowForceSubmitAlert,
  submitChecklist,
  showForceSubmitAlert,
}: ForceSubmitAlertProps) => {
  const { t } = useTranslation();
  const alertTitle = t("checklistDetail.form.forceSaveAlertTitle");
  const alertBody = t("checklistDetail.form.forceSaveAlertBody");
  const leftButton = (
    <Button
      onClick={() => setShowForceSubmitAlert(false)}
      variant="outline-dark"
      className="me-3 fs-8 px-3 py-1"
    >
      {t("checklistDetail.form.keepEditingBtn")}
    </Button>
  );
  const rightButton = (
    <Button
      onClick={() => submitChecklist(true)}
      variant="dark"
      className="fs-8 px-3 py-1"
    >
      {t("checklistDetail.form.forceSaveSubmitBtn")}
    </Button>
  );
  return (
    <Row>
      <Col>
        <CustomAlert
          className="mt-5"
          variant="warning"
          show={showForceSubmitAlert}
          alertTitle={alertTitle}
          alertBody={alertBody}
          showButtons={true}
          leftButton={leftButton}
          rightButton={rightButton}
        ></CustomAlert>
      </Col>
    </Row>
  );
};

const DueDate = ({ date }) => {
  const { t } = useTranslation();

  const days = daysUntil(date);
  const dateStr = dateString(date);
  if (days == 0) {
    return t("checklistDetail.dueDateDateToday", { date: dateStr });
  } else if (days < 0) {
    return t("checklistDetail.dueDateDateLate", {
      days: -days,
      date: dateStr,
    });
  } else {
    return t("checklistDetail.dueDateDate", { days: days, date: dateStr });
  }
};

const ChecklistSidebar = ({ checklist, showStatus }) => {
  const { t } = useTranslation();

  return (
    <>
      {showStatus && (
        <div className="checklist-sidebar-row">
          <Row>
            <small>{t("checklistDetail.status")}</small>
          </Row>
          <Row>
            <div
              className={`checklist-status ${checklist.status.toLowerCase()}`}
            >
              <small>
                {t(`checklistStatus.${checklist.status.toLowerCase()}`)}
              </small>
            </div>
          </Row>
        </div>
      )}
      <div className="checklist-sidebar-row">
        <Row>
          <small>{t("checklistDetail.assignee")}</small>
        </Row>
        <Row>
          <Col xs="auto">
            <MiniAvatar className="me-2" user={checklist.assignee} />
          </Col>
          <Col xs="auto">
            {(checklist.assignee as DisplayUser).display_name}
          </Col>
        </Row>
      </div>
      {checklist.run && (
        <>
          <div className="checklist-sidebar-row">
            <Row>
              <small>{t("checklistDetail.dueDate")}</small>
            </Row>
            <Row>
              <DueDate date={checklist.run.due_date} />
            </Row>
          </div>
          <div className="checklist-sidebar-row">
            <Row>
              <small>{t("checklistDetail.dateRange")}</small>
            </Row>
            <Row>
              {t("checklistDetail.dateRangeDate", {
                startDate: dateString(checklist.run.start_date, true),
                endDate: dateString(checklist.run.end_date),
              })}
            </Row>
          </div>
          <div className="checklist-sidebar-row">
            <Row>
              <small>{t("checklistDetail.team")}</small>
            </Row>
            <Row>{checklist.run.team_name}</Row>
          </div>
        </>
      )}
    </>
  );
};

export const ChecklistDetail = () => {
  const { t } = useTranslation();
  const { setNotification } = useNotification();
  const { auth } = useAuth();
  const username = auth?.username!;
  const location = useLocation();
  const fromMyLists = location?.state?.fromMyLists;
  const { checklistId } = useParams<ChecklistDetailRouteParams>();
  const checklistUrl = `${CHECKLIST_LIST_API}${checklistId}`;
  const [showForceSubmitAlert, setShowForceSubmitAlert] = useState(false);
  const [readOnly, setReadOnly] = useState(null);
  const [showMobileSidebar, setShowMobileSidebar] = useState(false);
  // checklist details
  const checklistController = useSingle<TaskList>([username, checklistId!], {
    endpoint: `${checklistUrl}/`,
  });

  // track submission count to update submit button text
  const submissionCount = useSingle<{ num: number }>(
    [checklistId!, "submissionCount"],
    {
      endpoint: "#",
      x: { num: 0 },
    },
  );

  const assignee = checklistController.x?.assignee;

  checklistController.getOnce();
  useEffect(() => {
    if (checklistController.ready) {
      setReadOnly(assignee.username !== auth?.username);
      // update checklistController on page number change to handle hidden completed tasks
      if (checklistController.x?.completed) {
        submissionCount.p.num.model = 1;
      }
    }
  }, [checklistController.ready]);

  // tasks list
  const tasksController = useList<Task>([checklistId!, "task"], {
    endpoint: `${checklistUrl}/task/`,
    paginated: false,
  });

  tasksController.getOnce();

  const sendReminder = useSingle<{}>([checklistId!, "sendReminder"], {
    endpoint: `${checklistUrl}/send_reminder/`,
  });

  // progress bar controller
  const progressValue = useSingle<ProgressState>("taskProgress", {
    endpoint: "#",
    ready: true,
    x: { val: 0 },
  });
  // confetti controller
  const confettiState = useSingle<ConfettiState>("confettiState", {
    endpoint: "#",
  });

  useSelector(() => {
    if (readOnly === null || readOnly) {
      return null;
    }
    // Subscribe to the specific changes in the subtasks we're interested in for the purpose of rerendering this
    // parent component. Namely, the ones tracking progress/completion.
    return JSON.stringify(
      tasksController.list.map((item) => [item.x.id, item.x.completed]),
    );
  });

  const checklistState = emptyChecklistStats();
  performTallies(checklistState, tasksController.list);

  // Keep progress at 0 if checklist is read-only.
  const progressPercent =
    readOnly === null || readOnly
      ? 0
      : (checklistState.requiredCompletedTasks /
          checklistState.requiredTotalTasks) *
        100;

  useEffect(() => {
    // Doing this in useEffect because React is displeased if we mutate state critical to components elsewhere
    // within the render function itself.
    progressValue.p.val.model = progressPercent;
  }, [progressPercent, progressValue.p.val]);

  const incompleteRequiredTasksLength = useRef<number>(
    checklistState.incompleteRequiredTasks.length,
  );
  // When a checklist is already completed, and a required task becomes
  // incomplete, the checklist becomes incomplete.
  useEffect(() => {
    if (readOnly === null || readOnly) {
      return;
    }
    const length = checklistState.incompleteRequiredTasks.length;
    // Check if the length has increased i.e. if a task already completed
    // has become incomplete.
    if (
      length > incompleteRequiredTasksLength.current &&
      (checklistController.x?.status === ChecklistStatus.COMPLETED ||
        checklistController.x?.completed === true)
    ) {
      checklistController.updateX({
        status: ChecklistStatus.IN_PROGRESS,
        completed: false,
      });
    }
    incompleteRequiredTasksLength.current = length;
  }, [readOnly, checklistState.incompleteRequiredTasks.length]);

  useLayoutEffect(() => {
    if (checklistController.p.completed.model || !showForceSubmitAlert) {
      return;
    }
    const target = document.querySelector(".required-missing");
    if (target) {
      target.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  }, [showForceSubmitAlert, checklistController.p.completed.model]);

  const pendingTasks = checklistState.incompleteRequiredTasks.length;
  if (!pendingTasks && showForceSubmitAlert) {
    setShowForceSubmitAlert(false);
  }

  const items_text =
    pendingTasks === 1
      ? t("checklistDetail.itemSingular")
      : t("checklistDetail.itemPlural");

  const submitChecklist = (force = false) => {
    if (pendingTasks > 0 && !force) {
      checklistState.incompleteRequiredTasks.forEach((task) => {
        task.p.completed.errors = [t("checklistDetail.form.pleaseComplete")];
      });
      setShowForceSubmitAlert(true);
      return;
    }
    checklistController.p.completed.errors = [];
    checklistController
      .patch({ completed: true, force_complete: force })
      .then((x) => {
        if (x.completed) {
          submissionCount.p.num.model += 1;
          confettiState.p.fire.model = Math.random();
        }
        checklistController.setX(x);
      })
      .catch((err) => {
        checklistController.p.completed.errors = err.response.data.completed;
        checklistController.p.completed.model = false;
      })
      .finally(() => {
        setShowForceSubmitAlert(false);
      });
  };

  const copyToClipboard = (text: string) => {
    navigator.clipboard.writeText(text);
    setNotification({
      message: t("checklists.notification.copiedToClipboard"),
      variant: "success",
    });
  };

  return (
    <LoadSection controllers={[checklistController]}>
      {() =>
        readOnly !== null && (
          <Container>
            <Row className="run-detail-header mb-5">
              <Col className="d-flex align-items-center">
                {fromMyLists ? (
                  <>
                    <span className="d-none d-sm-inline">
                      <Link
                        className="text-reset custom-breadcrumb"
                        to={urls.USER_CHECKLISTS}
                      >
                        {t("userChecklist.activeListTitle")}
                      </Link>
                      <span className="custom-breadcrumb-arrow" />
                    </span>
                    <Link
                      className="text-reset custom-breadcrumb d-sm-none me-2"
                      to={urls.USER_CHECKLISTS}
                    >
                      ‹
                    </Link>
                    <span className="text-reset">
                      {checklistController.x!.name}
                    </span>
                  </>
                ) : (
                  checklistController.x!.run && (
                    <>
                      <span className="d-none d-sm-inline">
                        <Link
                          className="text-reset custom-breadcrumb"
                          to={
                            checklistController.x!.run.is_archived
                              ? urls.ARCHIVED_RUNS
                              : urls.RUNS
                          }
                        >
                          {checklistController.x!.run.is_archived
                            ? t("runDetail.archivedRunsLink")
                            : t("runDetail.runsLink")}
                        </Link>
                        <span className="custom-breadcrumb-arrow" />
                        <Link
                          className="text-reset custom-breadcrumb"
                          to={`${urls.RUNS}/${checklistController.x!.run.id}/`}
                        >
                          {checklistController.x!.run.name}
                        </Link>
                        <span className="custom-breadcrumb-arrow" />
                      </span>
                      <Link
                        className="text-reset custom-breadcrumb d-sm-none me-2"
                        to={`${urls.RUNS}/${checklistController.x!.run.id}/`}
                      >
                        ‹
                      </Link>
                      <span className="text-reset">
                        {(assignee as DisplayUser).display_name}
                      </span>
                    </>
                  )
                )}
              </Col>
            </Row>
            <Row className="mb-4">
              <Col dir="auto">
                <h2 className="fw-bold">
                  <Title text={checklistController.x!.name} />
                </h2>
                <Body text={checklistController.x!.body} />
              </Col>
            </Row>
            <Row className="checklist-subheader d-lg-none align-items-center mx-0 py-2">
              <Col className="ps-0" xs="auto">
                <div
                  className={`checklist-status ${checklistController.x!.status.toLowerCase()}`}
                >
                  <small>
                    {t(
                      `checklistStatus.${checklistController.x!.status.toLowerCase()}`,
                    )}
                  </small>
                </div>
              </Col>
              <Col xs="auto">
                <Row>
                  {checklistController.x!.run && (
                    <DueDate date={checklistController.x!.run.due_date} />
                  )}
                </Row>
              </Col>
              <Col className="ms-auto pe-0" xs="auto">
                <Button
                  variant="secondary"
                  onClick={() => setShowMobileSidebar(true)}
                >
                  <DoubleChevronIcon style={{ transform: "rotate(180deg)" }} />
                </Button>
                <Offcanvas
                  className="checklist-mobile-sidebar d-lg-none"
                  backdropClassName="checklist-mobile-sidebar-backdrop d-lg-none"
                  scroll={true}
                  show={showMobileSidebar}
                  onHide={() => setShowMobileSidebar(false)}
                  placement="end"
                >
                  <Offcanvas.Header>
                    <Button
                      variant="secondary"
                      onClick={() => setShowMobileSidebar(false)}
                    >
                      <DoubleChevronIcon />
                    </Button>
                  </Offcanvas.Header>
                  <Offcanvas.Body>
                    <ChecklistSidebar
                      checklist={checklistController.x!}
                      showStatus={false}
                    />
                  </Offcanvas.Body>
                </Offcanvas>
              </Col>
            </Row>
            {!readOnly && (
              <Row className="my-2">
                <Col>
                  <span className="fs-8 text-dark-slate">
                    {t("checklistDetail.form.completeOnTotal", {
                      completedTasks: checklistState.completedTasks,
                      totalTasks: checklistState.totalTasks,
                    })}
                  </span>
                </Col>
              </Row>
            )}
            <Row>
              <Col xs={12} lg={9}>
                {readOnly ? (
                  <>
                    <div className="checklist-container read-only">
                      {checklistController.x!.completed ? (
                        checklistState.rootTasks.map((tc) => (
                          <Single key={tc.x!.id} controller={tc}>
                            {() => (
                              <TaskWithSubtasks
                                taskController={tc}
                                readOnly={true}
                                subtasks={checklistState.taskMap[tc.x.id]}
                              />
                            )}
                          </Single>
                        ))
                      ) : (
                        <div className="p-5">
                          <Row className="my-2">
                            <Col>
                              {t("checklistDetail.notSubmitted", {
                                assignee: (assignee as DisplayUser)
                                  .display_name,
                              })}
                            </Col>
                          </Row>
                          <Row className="my-2">
                            <Col>
                              <Button
                                variant="primary"
                                className="p-3"
                                onClick={() => sendReminder.post({})}
                              >
                                {t("checklistDetail.sendReminder")}
                              </Button>
                            </Col>
                          </Row>
                        </div>
                      )}
                    </div>
                    {checklistController.x!.completed && (
                      <Row className="mt-4">
                        <Col md={6}>
                          <Button
                            variant="primary"
                            className="p-3 fs-7"
                            onClick={() =>
                              copyToClipboard(
                                `${window.location.origin}/lists/${checklistId}/`,
                              )
                            }
                          >
                            <ShareListIcon
                              className="base-icon sm fill-dark me-2"
                              width="16px"
                              height="16px"
                              viewBox="0 0 16 16"
                            />
                            {t("checklistDetail.share", {
                              assignee: (assignee as DisplayUser).display_name,
                            })}
                          </Button>
                        </Col>
                      </Row>
                    )}
                  </>
                ) : (
                  <div className="checklist-container">
                    <Paginated controller={tasksController}>
                      <Accordion
                        defaultActiveKey={checklistState.tasksWithInteraction}
                        alwaysOpen
                        flush
                      >
                        {checklistState.rootTasks.map((tc) => (
                          <Single key={tc.x!.id} controller={tc}>
                            {() => (
                              <>
                                {tc.p.response.errors?.map((error) => (
                                  <Row key={error} className="mt-3">
                                    <Col>
                                      <CustomAlert
                                        className="pb-0 d-flex"
                                        variant="danger"
                                      >
                                        <p>{error}</p>
                                      </CustomAlert>
                                    </Col>
                                  </Row>
                                ))}
                                <TaskWithSubtasks
                                  taskController={tc}
                                  readOnly={false}
                                  subtasks={checklistState.taskMap[tc.x.id]}
                                />
                              </>
                            )}
                          </Single>
                        ))}
                      </Accordion>
                    </Paginated>
                    {!checklistController.p.completed.errors?.length &&
                      checklistController.p.completed.model && (
                        <Row className="mt-3">
                          <Col>
                            <CustomAlert
                              className="pb-0 d-flex"
                              variant="success"
                            >
                              <p>
                                {t("checklistDetail.form.checklistCompleted")}
                              </p>
                            </CustomAlert>
                          </Col>
                        </Row>
                      )}
                    {checklistController.p.completed.errors?.map((error) => (
                      <Row key={error} className="mt-3">
                        <Col>
                          <CustomAlert className="pb-0 d-flex" variant="danger">
                            <p>{error}</p>
                          </CustomAlert>
                        </Col>
                      </Row>
                    ))}
                    <ForceSubmitAlert
                      setShowForceSubmitAlert={setShowForceSubmitAlert}
                      showForceSubmitAlert={showForceSubmitAlert}
                      submitChecklist={submitChecklist}
                    />
                    {!showForceSubmitAlert &&
                      (!checklistController.p.completed.model ||
                        checklistController.p.completed.errors?.length !==
                          0) && (
                        <div>
                          <Row className="mt-4 pt-3">
                            <p>
                              <Trans i18nKey="checklistDetail.form.autoSaveNotice" />
                            </p>
                          </Row>
                          <Row className="mt-2">
                            <Col md={6}>
                              <Button
                                variant="primary"
                                onClick={() => submitChecklist()}
                                disabled={checklistState.failed}
                              >
                                {submissionCount.p.num.model > 0
                                  ? t("checklistDetail.form.submitBtnAgain")
                                  : t("checklistDetail.form.submitBtn")}
                              </Button>
                            </Col>
                          </Row>
                          {pendingTasks > 0 && (
                            <Row className="mt-3">
                              <p className="fs-8">
                                {t("checklistDetail.form.pendingTasks", {
                                  pendingTasks,
                                  itemsText: items_text,
                                })}
                              </p>
                            </Row>
                          )}
                        </div>
                      )}
                  </div>
                )}
              </Col>
              <Col className="checklist-sidebar d-none d-lg-block" lg={3}>
                <div className="ms-3">
                  <ChecklistSidebar
                    checklist={checklistController.x!}
                    showStatus={true}
                  />
                </div>
              </Col>
            </Row>
          </Container>
        )
      }
    </LoadSection>
  );
};
