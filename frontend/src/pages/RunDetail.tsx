import { useList, useSingle } from "@opencraft/providence-redux/hooks";
import { Col, Dropdown, Nav, Row } from "react-bootstrap";
import { RunChecklistsTable } from "../components/RunChecklistsTable";
import { Run, RunArchiveAction, TaskListReview } from "../types/Task";
import { Link, useParams, useSearchParams } from "react-router-dom";
import Container from "react-bootstrap/Container";
import useAuth from "../hooks/useAuth";
import useNotification from "../hooks/useNotification";
import { useTranslation } from "react-i18next";
import { Divider } from "../components/Divider";
import { useEffect, useState } from "react";
import { MemberOption, MemberFilter } from "../components/MemberFilter";
import { StatusCustomOption, StatusFilter } from "../components/StatusFilter";
import ArchiveIcon from "../assets/icons/archive.svg?react";
import UnarchiveIcon from "../assets/icons/unarchive.svg?react";
import ShareListIcon from "../assets/icons/share-list.svg?react";
import ChevronDownIcon from "../assets/icons/chevron-down.svg?react";
import CrossIcon from "../assets/icons/cross.svg?react";
import { RUN_LIST_API } from "../constants/api-urls";
import { ActionMeta, MultiValue } from "react-select";
import { isEqual } from "lodash";
import { Title } from "../components/Title";
import { RunSubtitle } from "../components/RunSubtitle";
import { LoadSection } from "../components/LoadSection";
import * as urls from "../constants/urls";

type RunDetailRouteParams = {
  runId: string;
};

type GetAction = "filter" | "init";

interface GetChecklistArgs {
  action: GetAction;
}

export const RunDetail = () => {
  const { runId } = useParams<RunDetailRouteParams>();
  const runController = useSingle<Run>(["run", runId!], {
    endpoint: `${RUN_LIST_API}${runId}/`,
  });
  const checklists = useList<TaskListReview>(["run", runId!, "checklists"], {
    endpoint: `${RUN_LIST_API}${runId}/checklists/`,
  });
  const { t } = useTranslation();
  const { auth } = useAuth();
  const copyToClipboard = (text: string) => {
    navigator.clipboard.writeText(text);
    setNotification({
      message: t("runs.notification.copiedToClipboard"),
      variant: "success",
    });
  };
  const currentUsername = auth?.username!;
  const [searchParams, setSearchParams] = useSearchParams();
  const [isInitDone, setIsInitDone] = useState<boolean>(false);
  const [selectedStatuses, setSelectedStatuses] = useState<Array<string>>(
    searchParams.getAll("status"),
  );
  const [selectedUsernames, setSelectedUsernames] = useState<Array<string>>(
    searchParams.getAll("username"),
  );
  const page = searchParams.get("page") || "1";
  const size = searchParams.get("size") || "24";

  const isFiltering = (): boolean => {
    return selectedUsernames.length > 0 || selectedStatuses.length > 0;
  };

  const reset = () => {
    setSelectedStatuses([]);
    setSelectedUsernames([]);
  };

  const getChecklists = ({ action }: GetChecklistArgs) => {
    if (action === "init") {
      runController.get();
    }
    let pageParam = page;
    if (action === "filter") {
      pageParam = "1";
      const urlSearchParam = new URLSearchParams(searchParams);
      urlSearchParam.set("page", pageParam);
      setSearchParams(urlSearchParam);
    }
    let params = {
      page: pageParam,
      size: size,
    };
    if (selectedStatuses.length > 0) {
      params["statuses"] = selectedStatuses.join(",");
    }
    if (selectedUsernames.length > 0) {
      params["usernames"] = selectedUsernames.join(",");
    }
    if (!isEqual(checklists.params, params) || action === "init") {
      checklists.params = params;
      checklists.get();
    }
  };

  const setQueryParams = (
    queryParams: { name: string; values: Array<string | undefined> }[],
  ) => {
    const urlSearchParams = new URLSearchParams(searchParams);
    for (const queryParam of queryParams) {
      urlSearchParams.delete(queryParam.name);
      for (const value of queryParam.values) {
        if (!!value) {
          urlSearchParams.append(queryParam.name, value);
        }
      }
    }
    setSearchParams(urlSearchParams);
  };

  const onStatusFilterChange = (
    values: MultiValue<StatusCustomOption>,
  ): void => {
    const statuses: Array<string> = [];
    for (const statusOption of values) {
      statuses.push(statusOption.value);
    }
    setQueryParams([{ name: "status", values: statuses }]);
    setSelectedStatuses(statuses);
  };

  const onMemberFilterChange = (
    values: MultiValue<MemberOption>,
    actionMeta: ActionMeta<MemberOption>,
  ): void => {
    const usernameSet: Set<string> = new Set();
    for (const memberOption of values) {
      usernameSet.add(memberOption.value);
    }
    if (actionMeta.action === "deselect-option") {
      usernameSet.delete(actionMeta.option!.value);
    }
    const usernames = Array.from(usernameSet);
    setQueryParams([{ name: "username", values: Array.from(usernames) }]);
    setSelectedUsernames(usernames);
  };

  const { setNotification } = useNotification();
  const handleFailedArchiveUnarchive = (error: any) => {
    let message = t("runs.notification.error.undefined");
    if (error.response?.status === 403) {
      message = t("runs.notification.error.forbidden");
    }
    setNotification({
      message,
      variant: "info",
    });
  };
  const onArchiveUnarchive = (isArchived: boolean) => {
    const action = isArchived
      ? RunArchiveAction.ARCHIVE
      : RunArchiveAction.UNARCHIVE;
    runController
      .patch({ is_archived: isArchived })
      .then(() => {
        setNotification({
          message: t(`runs.notification.${action.toLowerCase()}`),
          variant: "success",
        });
        runController.get();
      })
      .catch(handleFailedArchiveUnarchive);
  };

  useEffect(() => {
    let action: GetAction = "filter";
    // to prevent reset page size in the first time page is loaded
    if (!isInitDone) {
      action = "init";
      setIsInitDone(true);
    }
    getChecklists({ action });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedUsernames, selectedStatuses]);

  return (
    <LoadSection controllers={[runController]}>
      {() => (
        <Container className="lists-page">
          <Row className="run-detail-header mb-5">
            <Col className="d-flex align-items-center">
              <span className="d-none d-sm-inline">
                <Link
                  className="text-reset custom-breadcrumb"
                  to={
                    runController.x!.is_archived
                      ? urls.ARCHIVED_RUNS
                      : urls.RUNS
                  }
                >
                  {runController.x!.is_archived
                    ? t("runDetail.archivedRunsLink")
                    : t("runDetail.runsLink")}
                </Link>
                <span className="custom-breadcrumb-arrow" />
              </span>
              <Link
                className="text-reset custom-breadcrumb d-sm-none me-2"
                to={
                  runController.x!.is_archived ? urls.ARCHIVED_RUNS : urls.RUNS
                }
              >
                ‹
              </Link>
              <span className="text-reset">{runController.x!.name}</span>
            </Col>
            <Col className="d-flex align-items-center" xs="auto">
              <Dropdown>
                <Dropdown.Toggle className="px-2 py-1 fs-8 run-detail-action-button">
                  {t("runDetail.actionsButton")}
                  <ChevronDownIcon className="base-icon sm ms-1" />
                </Dropdown.Toggle>
                <Dropdown.Menu align="end">
                  {!runController.x!.is_archived && (
                    <Dropdown.Item
                      className="text-reset"
                      onClick={() =>
                        copyToClipboard(
                          `${window.location.origin}/runs/${runId}/`,
                        )
                      }
                    >
                      <ShareListIcon className="base-icon sm me-2" />
                      <span className="mb-0">{t("runs.action.copyLink")}</span>
                    </Dropdown.Item>
                  )}
                  <Dropdown.Item
                    className="text-reset"
                    onClick={() =>
                      onArchiveUnarchive(!runController.x!.is_archived)
                    }
                  >
                    {runController.x!.is_archived ? (
                      <UnarchiveIcon className="base-icon sm me-2" />
                    ) : (
                      <ArchiveIcon className="base-icon sm me-2" />
                    )}
                    <span className="mb-0">
                      {runController.x!.is_archived
                        ? t("runs.action.unarchive")
                        : t("runs.action.archive")}
                    </span>
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
          <Row>
            <Col>
              <Row>
                <h1 className="fw-bold">
                  <Title text={runController.x!.name} />
                </h1>
              </Row>
              <Row>
                <RunSubtitle run={runController.x!} />
              </Row>
            </Col>
            <Col className="pt-2" xs={12} sm="auto">
              <div className="d-flex justify-content-end">
                <StatusFilter
                  selectedStatuses={selectedStatuses}
                  onChange={onStatusFilterChange}
                  className="ms-3"
                  isLoading={checklists.fetching}
                />
                <MemberFilter
                  className="ms-3"
                  onChange={onMemberFilterChange}
                  selectedMemberUsernames={selectedUsernames}
                  isLoading={checklists.fetching}
                  showTeams={false}
                />
                {isFiltering() && (
                  <Nav.Link
                    className="text-reset p-0 ms-2"
                    onClick={() => reset()}
                  >
                    <CrossIcon className="base-icon sm" />
                  </Nav.Link>
                )}
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <Divider />
            </Col>
          </Row>
          <Row>
            <Col>
              <RunChecklistsTable
                controller={checklists}
                currentUsername={currentUsername}
              />
            </Col>
          </Row>
        </Container>
      )}
    </LoadSection>
  );
};
