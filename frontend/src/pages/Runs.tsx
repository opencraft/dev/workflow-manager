import { useList } from "@opencraft/providence-redux/hooks";
import { AccordionButton, Col, Dropdown, Nav, Row } from "react-bootstrap";
import { RunsTable } from "../components/RunsTable";
import { Run, RunArchiveAction } from "../types/Task";
import { useNavigate, useSearchParams } from "react-router-dom";
import Container from "react-bootstrap/Container";
import { useTranslation } from "react-i18next";
import { useCallback, useEffect, useState } from "react";
import {
  MemberCustomOption,
  MemberFilter,
  MemberOptionType,
} from "../components/MemberFilter";
import ListIcon from "../assets/icons/list.svg?react";
import ArchiveIcon from "../assets/icons/archive.svg?react";
import CrossIcon from "../assets/icons/cross.svg?react";
import { ToggleSearchBar } from "../components/ToggleSearchBar";
import { RUN_LIST_API } from "../constants/api-urls";
import { ActionMeta, MultiValue } from "react-select";
import _, { isEqual } from "lodash";
import useNotification from "../hooks/useNotification";

declare interface RunsProps {
  isArchived?: boolean;
}

type GetAction = "filter" | "refresh" | "init";

interface GetRunArgs {
  action: GetAction;
}

export const Runs = ({ isArchived }: RunsProps) => {
  const archivedName = isArchived ? "archived" : "active";
  const allRuns = useList<Run>(["runs", archivedName], {
    endpoint: RUN_LIST_API,
  });

  const { t } = useTranslation();
  const [searchParams, setSearchParams] = useSearchParams();
  const [isInitDone, setIsInitDone] = useState<boolean>(false);
  const [selectedTeamIds, setSelectedTeamIds] = useState<Array<string>>(
    searchParams.getAll("team"),
  );
  const [selectedUsernames, setSelectedUsernames] = useState<Array<string>>(
    searchParams.getAll("username"),
  );
  const [searchText, setSearchText] = useState<string>(
    searchParams.get("name") || "",
  );
  const page = searchParams.get("page") || "1";
  const size = searchParams.get("size") || "24";
  const isFiltering = (): boolean => {
    return selectedUsernames.length > 0 || searchText.length > 0;
  };

  const reset = () => {
    setSelectedTeamIds([]);
    setSelectedUsernames([]);
    setSearchText("");
  };

  const getRuns = ({ action }: GetRunArgs) => {
    let pageParam = page;
    if (action === "filter") {
      pageParam = "1";
      const urlSearchParam = new URLSearchParams(searchParams);
      urlSearchParam.set("page", pageParam);
      setSearchParams(urlSearchParam);
    }
    let params = {
      is_archived: isArchived,
      page: pageParam,
      size: size,
    };
    if (selectedTeamIds.length > 0) {
      params["teams"] = selectedTeamIds.join(",");
    }
    if (selectedUsernames.length > 0) {
      params["usernames"] = selectedUsernames.join(",");
    }
    if (searchText.length > 0) {
      params["name"] = searchText;
    }
    if (!isEqual(allRuns.params, params) || action === "refresh") {
      allRuns.params = params;
      allRuns.get();
    }
  };

  const setQueryParams = (
    queryParams: { name: string; values: Array<string | undefined> }[],
  ) => {
    const urlSearchParams = new URLSearchParams(searchParams);
    for (const queryParam of queryParams) {
      urlSearchParams.delete(queryParam.name);
      for (const value of queryParam.values) {
        if (!!value) {
          urlSearchParams.append(queryParam.name, value);
        }
      }
    }
    setSearchParams(urlSearchParams);
  };

  useEffect(() => {
    let action: GetAction = "filter";
    // to prevent reset page size in the first time page is loaded
    if (!isInitDone) {
      action = "init";
      setIsInitDone(true);
    }
    getRuns({ action });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTeamIds, selectedUsernames, searchText]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounceSearchTextChange = useCallback(
    _.debounce((value: string) => setSearchText(value), 500),
    [],
  );
  const onSearchTextChange = (newValue: string) => {
    const urlSearchParams = new URLSearchParams(searchParams);
    urlSearchParams.delete("name");
    if (newValue !== "") {
      urlSearchParams.set("name", newValue);
    }
    setSearchParams(urlSearchParams);
    debounceSearchTextChange(newValue);
  };

  const onMemberFilterChange = (
    values: MultiValue<MemberCustomOption>,
    actionMeta: ActionMeta<MemberCustomOption>,
  ): void => {
    const teamIdSet: Set<string> = new Set();
    const usernameSet: Set<string> = new Set();
    for (const memberOption of values) {
      if (memberOption.type === MemberOptionType.TEAM) {
        teamIdSet.add(memberOption.value);
      }
      if (memberOption.type === MemberOptionType.MEMBER) {
        usernameSet.add(memberOption.value);
      }
    }
    if (actionMeta.action === "deselect-option") {
      switch (actionMeta.option?.type) {
        case MemberOptionType.TEAM:
          teamIdSet.delete(actionMeta.option!.value);
          break;
        case MemberOptionType.MEMBER:
          usernameSet.delete(actionMeta.option!.value);
          break;
        default:
          break;
      }
    }
    const teamIds = Array.from(teamIdSet);
    const usernames = Array.from(usernameSet);
    setQueryParams([
      { name: "team", values: Array.from(teamIds) },
      { name: "username", values: Array.from(usernames) },
    ]);
    setSelectedTeamIds(teamIds);
    setSelectedUsernames(usernames);
  };

  const { setNotification } = useNotification();
  const handleFailedArchiveUnarchive = (error: any) => {
    let message = t("runs.notification.error.undefined");
    if (error.response?.status === 403) {
      message = t("runs.notification.error.forbidden");
    }
    setNotification({
      message,
      variant: "info",
    });
  };
  const onArchiveUnarchive = (id: string, isArchived: boolean) => {
    const action = isArchived
      ? RunArchiveAction.ARCHIVE
      : RunArchiveAction.UNARCHIVE;
    const controller = allRuns.list.filter((c) => c.x!.id === id)[0];
    controller
      .patch({ is_archived: isArchived })
      .then(() => {
        setNotification({
          message: t(`runs.notification.${action.toLowerCase()}`),
          variant: "success",
        });
        getRuns({ action: "refresh" });
      })
      .catch(handleFailedArchiveUnarchive);
  };

  const navigate = useNavigate();
  const switchToList = (toIsArchived: boolean = true) => {
    let path = "/runs";
    if (toIsArchived) {
      path = "/archived-runs";
    }
    searchParams.set("page", "1");
    navigate({
      pathname: path,
      search: searchParams.toString(),
    });
  };
  return (
    <Container className="runs-page">
      <Row className="run-listing-header" xs={1} sm={2}>
        <Col>
          <Dropdown className="d-flex">
            <Dropdown.Toggle
              className="px-0 py-0 text-reset accordion"
              as={Nav.Link}
              to="#"
              split
              id="dropdown-split-basic"
            >
              <AccordionButton className="large">
                <h1 className="fw-bold mb-0 me-3">
                  {isArchived
                    ? t("runs.archivedRunsTitle")
                    : t("runs.activeRunsTitle")}
                </h1>
              </AccordionButton>
            </Dropdown.Toggle>
            <Dropdown.Menu className="p-2 border">
              <Dropdown.Item onClick={() => switchToList(false)}>
                <Row>
                  <Col xs={2}>
                    <ListIcon className="base-icon circle line rect sm" />
                  </Col>
                  <Col xs={10}>
                    <span className="fw-bold">{t("runs.activeRunsTitle")}</span>
                  </Col>
                </Row>
              </Dropdown.Item>
              <Dropdown.Item onClick={() => switchToList(true)}>
                <Row>
                  <Col xs={2}>
                    <ArchiveIcon className="base-icon sm path" />
                  </Col>
                  <Col xs={10}>
                    <span className="fw-bold">
                      {t("runs.archivedRunsTitle")}
                    </span>
                  </Col>
                </Row>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Col>
        <Col className="pt-2">
          <div className="d-flex justify-content-end list-filters">
            <ToggleSearchBar
              value={searchText}
              onChange={onSearchTextChange}
              className="mx-3"
            />
            <MemberFilter
              className="ms-3"
              onChange={onMemberFilterChange}
              selectedMemberUsernames={selectedUsernames}
              selectedTeamIds={selectedTeamIds}
              isLoading={allRuns.fetching}
            />
            {isFiltering() && (
              <Nav.Link className="text-reset p-0 ms-2" onClick={() => reset()}>
                <CrossIcon className="base-icon sm" />
              </Nav.Link>
            )}
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <RunsTable
            controller={allRuns}
            onArchiveUnarchive={onArchiveUnarchive}
          />
        </Col>
      </Row>
    </Container>
  );
};
