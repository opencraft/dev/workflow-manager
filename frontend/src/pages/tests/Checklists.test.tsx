import { Checklists } from "../Checklists";
import {
  CHECKLIST_COUNT_API,
  CHECKLIST_LIST_API,
} from "../../constants/api-urls";
import { customRender, mockAxios } from "../../utils/test-utils";
import { expect, vi, describe, it, beforeAll, beforeEach } from "vitest";

const testCounts = {
  active: {
    TO_DO: 0,
    ASSIGNED_TO_ME: 3,
    ALL: 4,
  },
  archived: {
    TO_DO: 0,
    ASSIGNED_TO_ME: 2,
    ALL: 3,
  },
};

const defaultParams = {
  name: "",
  page: "1",
  size: "24",
  statuses: "",
  usernames: "",
};

describe("Checklists.tsx", () => {
  const mockRequests = () => {
    mockAxios.onGet(CHECKLIST_LIST_API).reply(200, { results: [] });
    mockAxios.onGet(CHECKLIST_COUNT_API).reply(200, testCounts);
  };

  describe("Default mode", () => {
    let result;
    beforeAll(async () => {
      mockRequests();
      mockAxios.resetHistory();
      result = await customRender(<Checklists isArchived={false} />);
    });

    it("Loads the ASSIGNED_TO_ME list", () => {
      expect(mockAxios.history.get).toContainEqual(
        expect.objectContaining({
          url: CHECKLIST_LIST_API,
          params: {
            ...defaultParams,
            is_archived: false,
            list_name: "ASSIGNED_TO_ME",
          },
        }),
      );
    });

    it("Loads list counts", () => {
      expect(mockAxios.history.get).toContainEqual(
        expect.objectContaining({
          url: CHECKLIST_COUNT_API,
        }),
      );
    });
  });

  describe("Archived mode", () => {
    let result;
    beforeAll(async () => {
      mockRequests();
      mockAxios.resetHistory();
      result = await customRender(<Checklists isArchived={true} />);
    });

    it("Loads the ASSIGNED_TO_ME list", () => {
      expect(mockAxios.history.get).toContainEqual(
        expect.objectContaining({
          url: CHECKLIST_LIST_API,
          params: {
            ...defaultParams,
            is_archived: true,
            list_name: "ASSIGNED_TO_ME",
          },
        }),
      );
    });
  });
});
