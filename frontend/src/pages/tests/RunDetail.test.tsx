import {
  customRender,
  genRun,
  mockAxios,
  testContext,
} from "../../utils/test-utils";
import { RunDetail } from "../RunDetail";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { ChecklistStatus } from "../../types/Task";
import { expect, vi, describe, it, beforeEach } from "vitest";
import { RUN_LIST_API } from "../../constants/api-urls";

vi.mock("../../components/MemberFilter", () => ({
  MemberFilter: () => <div data-testid="member-filter" />,
}));

let store: IModuleStore<any>;
let context: GlobalOptions;

describe("RunDetail page", () => {
  const runId = "test_run_id";
  const user1 = {
    id: 1,
    username: "testuser1",
    email: "test1@example.com",
  };
  const user2 = {
    id: 2,
    username: "testuser2",
    email: "test2@example.com",
  };
  const run = genRun({
    assignees: [
      {
        ...user1,
        display_name: "Test User 1",
      },
      {
        ...user2,
        display_name: "Test User 2",
      },
    ],
  });
  const checklists = [
    {
      id: "test_checklist_id_1",
      name: "Test Checklist",
      body: "test body 1",
      assignee: user1,
      created_on: "2024-03-07T12:00:00Z",
      completed: false,
      completed_on: null,
      run: run,
      completed_task_count: 5,
      total_task_count: 10,
      status: ChecklistStatus.IN_PROGRESS,
      is_archived: false,
    },
    {
      id: "test_checklist_id_2",
      name: "Test Checklist",
      body: "test body 2",
      assignee: user2,
      created_on: "2024-03-07T12:00:00Z",
      completed: false,
      completed_on: null,
      run: run,
      completed_task_count: 5,
      total_task_count: 10,
      status: ChecklistStatus.IN_PROGRESS,
      is_archived: false,
    },
  ];
  const initialEntries = [`/runs/${runId}/`];
  const paths = ["/runs/:runId"];

  beforeEach(() => {
    mockAxios.onGet(`${RUN_LIST_API}${runId}/`).reply(200, run);
    mockAxios
      .onGet(`${RUN_LIST_API}${runId}/checklists/`)
      .reply(200, { results: checklists });

    store = createStore({});
    context = testContext();
  });

  it("renders the run checklists", async () => {
    const result = customRender(<RunDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const checklist1 = await result.findAllByText("testuser1");
    expect(checklist1[0]).toBeInTheDocument();
    const checklist2 = result.getAllByText("testuser2");
    expect(checklist2[0]).toBeInTheDocument();
  });

  it("links to the runs page on unarchived runs", async () => {
    const result = customRender(<RunDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const runsLink = await result.findByText("runDetail.runsLink");
    expect(runsLink).toBeInTheDocument();
  });

  it("links to the archived runs page on archived runs", async () => {
    mockAxios
      .onGet(`${RUN_LIST_API}${runId}/`)
      .reply(200, { ...run, is_archived: true });

    const result = customRender(<RunDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const archivedRunsLink = await result.findByText(
      "runDetail.archivedRunsLink",
    );
    expect(archivedRunsLink).toBeInTheDocument();
  });
});
