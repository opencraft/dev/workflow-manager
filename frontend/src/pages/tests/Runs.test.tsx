import { customRender, mockAxios } from "../../utils/test-utils";
import { Runs } from "../Runs";
import { vi, describe, it, beforeEach } from "vitest";

vi.mock("../../components/MemberFilter", () => ({
  MemberFilter: () => <div data-testid="member-filter" />,
}));

vi.mock("../../components/RunsTable", () => ({
  RunsTable: () => <div data-testid="runs-table" />,
}));

describe("Runs.tsx", () => {
  beforeEach(() => {
    mockAxios.onGet("/api/workflow/checklist/run/").reply(200, { results: [] });
  });

  it("Shows runs table and members filter", async () => {
    const result = customRender(<Runs isArchived={false} />);
    await result.findByTestId("runs-table");
    await result.findByTestId("member-filter");
  });
});
