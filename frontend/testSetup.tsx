import { afterEach, vi } from 'vitest';
import { cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/vitest';
import {TransProps} from "react-i18next";
import React from "react";
import {flushPromises, mockAxios} from './src/utils/test-utils';
import ResizeObserverMock from 'resize-observer-polyfill'

afterEach(async () => {
    await flushPromises()
    cleanup();
    mockAxios.resetHandlers()
});

vi.mock("react-i18next")

// Stub the global ResizeObserver
vi.stubGlobal('ResizeObserver', ResizeObserverMock);

if (typeof window.URL.createObjectURL === 'undefined') {
    Object.defineProperty(window.URL, 'createObjectURL', { value: vi.fn()})
    Object.defineProperty(window.URL, 'revokeObjectURL', { value: vi.fn()})
}

// i18n overrides
vi.mock("react-i18next", async (importOriginal) => {
    return {
        ...await importOriginal<typeof import('react-i18next')>(),
        useTranslation: () => {
            return {
                t: (str: string) => str,
                i18n: {
                    changeLanguage: () => new Promise(() => {}),
                    dir: () => window.TEXT_DIRECTION,
                },
            };
        },
        Trans: (prop: TransProps<any>) => (
          <React.Fragment>{prop.children}</React.Fragment>
        ),
    }
});

const originalError = console.error

console.error = (...args) => {
    const main = args[0]
    if (typeof main === "string") {
        // Squash some warnings that apply to upstream and not us.
        if (/Support for defaultProps will be removed from function/.test(main)) {
            // This error is from recharts, which relies on a deprecated feature of one of its libraries (React?) as
            // of recharts 2.12.7. Next time recharts is updated, try removing this and seeing if the warning is gone.
            // if it is, please remove this hack.
            return
        }
    }
    originalError(...args)
}

declare global {
    interface Window {
        TEXT_DIRECTION: "ltr" | "rtl";
    }
}

window.TEXT_DIRECTION = "ltr";

// https://github.com/vitest-dev/vitest/issues/821
Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: vi.fn(),
    removeListener: vi.fn(),
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
})
