import path from 'path'
import svgr from 'vite-plugin-svgr'
import react from '@vitejs/plugin-react'
import checker from 'vite-plugin-checker'

export default {
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
    }
  },
  build: {
    outDir: './build',
    assetsDir: 'static',
    emptyOutDir: true,
  },
  test: {
    environment: 'jsdom',
    setupFiles: ['./testSetup.tsx'],
    testMatch: ['./tests/**/*.test.tsx?'],
  },
  plugins: [svgr(), react(), checker({typescript: true})],
  envPrefix: 'REACT_APP_',
  server: {
    port: 3000,
    hot: true,
  }
}
